import { LitElement, html, css } from 'lit';
import { customElement, property, state } from 'lit/decorators.js';

const ELEMENT_NAME = "kj-tabber-tab"

@customElement(ELEMENT_NAME)
export default class TabberTab extends LitElement {

    static styles = [
        css`
        :host {
            position: relative;
            /*inset: 0;
            /*width: 100%;
            height: 100%;*/

            background: var(--kj-tabber-tab--background, transparent);
        }
        :host {
            padding: 4px 6px;
            border: 1px solid #555;
            min-width: 100px;

            background:   var(--kj-tabber--item-header--background, #aaa);
            border:       var(--kj-tabber--item-header--border, 1px solid #444);
            border-color: var(--kj-tabber--item-header--border-color, #444);
            box-shadow:   var(--kj-tabber--item-header--box-shadow, none);

            /*z-index: 1;*/
        }
        :host(:hover) {
            background:   var(--kj-tabber--item-header--hover--background, #ccc);
            border-color: var(--kj-tabber--item-header--hover--border-color, #888);
            box-shadow:   var(--kj-tabber--item-header--hover--box-shadow, none);
        }
        :host(:active) {
            background:   var(--kj-tabber--item-header--active--background, #ccc);
            border-color: var(--kj-tabber--item-header--active--border-color, #888);
            box-shadow:   var(--kj-tabber--item-header--active--box-shadow, none);
        }
        :host([selected]) {
            background:   var(--kj-tabber--item-header--active--background, #ccc);
            border-color: var(--kj-tabber--item-header--active--border-color, #888);
        }
        `
    ];

    constructor() {
        super();
        //this.tabIndex = 0;
    }

    @property({ type: Boolean, reflect: true }) selected = false;

    @property({ type: String, reflect: true }) name = "";


    render() {
        return html`<slot></slot>`
    }

    attributeChangedCallback(name: string, oldVal: string, newVal: string) {

        if (name === "selected" && newVal !== null) {
            //console.log("selected", { oldVal, newVal });
            this.dispatchEvent(new Event("selected", { bubbles: true, cancelable: true, composed: true }))
        }
        super.attributeChangedCallback(name, oldVal, newVal);
    }

}

declare global {
    interface HTMLElementTagNameMap {
        [ELEMENT_NAME]: TabberTab;
    }
}