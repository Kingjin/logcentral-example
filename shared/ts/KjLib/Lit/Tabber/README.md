# Tabber

## Описание

**Tabber** - "многостраничный" контейнер с закладками.

## HTML syntax
```html
<kj-tabber>

    <kj-tabber-head>
        <kj-tabber-tab></kj-tabber-tab>
        ...
    </kj-tabber-head>

    <kj-tabber-body>
        <kj-tabber-page></kj-tabber-page>
        ...
    </kj-tabber-body>
</kj-tabber>
```
## Example

```html
<kj-tabber id="tab">
    <kj-tabber-head>
        <kj-tabber-tab name="tab1" selected>Tab 1</kj-tabber-tab>
        <kj-tabber-tab name="tab2">Tab 2</kj-tabber-tab>
        <kj-tabber-tab name="tab3">Tab 3</kj-tabber-tab>
    </kj-tabber-head>

    <kj-tabber-body>
        <kj-tabber-page name="tab1">
            Page 1
        </kj-tabber-page>

        <kj-tabber-page name="tab2">
            Page 2
        </kj-tabber-page>

        <kj-tabber-page name="tab3">
            Page 3
        </kj-tabber-page>
    </kj-tabber-body>
</kj-tabber>
```

### Properties and attributes

#### Tabber

| Property | Attribute | Description |
| --- | --- | --- |
| `position` | `position` | Position of TabberHead: <ins>"top"</ins> \| "bottom" \| "left" \| "right"


### Events

| Event | Description |
| --- | --- |
| `change` | Fire on toggle state changed