import { LitElement, html, css } from 'lit';
import { customElement, property, state } from 'lit/decorators.js';

import TabberTab  from "./TabberTab.js"

const ELEMENT_NAME = "kj-tabber-head"

@customElement(ELEMENT_NAME)
export default class TabberHead extends LitElement {

    static styles = [
        css`
        :host {
            position: relative;
            background: var(--kj-tabber-head--background, transparent);
        }`
    ];


    @state() protected _selectedTab: TabberTab = null;

    constructor() {
        super();
        this.__onClick_tab = this.__onClick_tab.bind(this);
    }


    render() {
        return html`<slot @slotchange=${ this.__onSlotChange }></slot>`
    }

    protected __onSlotChange(ev: Event) {
        console.log("__onSlotChange_head");

        this._selectedTab = null;

        const els = (<HTMLSlotElement>ev.target).assignedElements();
        for (let el of els) {
            if (el instanceof TabberTab) {
                const tab: TabberTab = el;
                tab.addEventListener("click", this.__onClick_tab);

                if (tab.selected) {
                    this.__selectItem(tab)
                }

                tab.addEventListener("selected", ev => {
                    this.__selectItem(tab);
                    //console.log(ev);
                    //ev.stopPropagation();
                });
            } else {
                el.remove()
            }
        }

    }

    protected __onClick_tab(ev: Event) {
        this.__selectItem(<TabberTab>ev.currentTarget);
    }

    /** Выбор элемента */
    protected __selectItem(tab: TabberTab) {
        if (this._selectedTab === tab) return; // возврат, если элементы совпадают

        if (this._selectedTab) {
            this._selectedTab.selected = false;
        }

        this._selectedTab = tab;
        this._selectedTab.selected = true;

        this.dispatchEvent(new Event("change"))
    }

    get selectedTab(): TabberTab {
        return this._selectedTab;
    }
}

declare global {
    interface HTMLElementTagNameMap {
        [ELEMENT_NAME]: TabberHead;
    }
}