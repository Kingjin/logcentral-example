import { LitElement, html, css } from 'lit';
import { customElement, property, state } from 'lit/decorators.js';

import "./TabberHead.js"
import "./TabberBody.js"
import "./TabberTab.js"
import "./TabberPage.js"

import TabberHead from "./TabberHead.js"
import TabberBody from "./TabberBody.js"
import TabberTab  from "./TabberTab.js"
import TabberPage from "./TabberPage.js"


@customElement("kj-tabber")
export default class Tabber extends LitElement {

    static styles = [
        css`
        :host {
            /*display: inline-flex;*/
            position: relative;
            text-shadow: none;
            /*transition: all 300ms ease;*/

            font-family: inherit;

            box-sizing: border-box;
            cursor: pointer;
        }

        #container {
            display: flex;
            width: 100%;
            height: 100%;
            /*background: var(--kj-tab--background, #aaa);*/
        }

        ::slotted(kj-tabber-head) {
            display: flex;
        }
        ::slotted(kj-tabber-body) {
            flex: 1;
            /*background: var(--kj-tab--background, #aaa);*/
        }

        :host([position="top"]) > #container {
            flex-direction: column;
        }
        :host([position="top"]) ::slotted(kj-tabber-head) {
            flex-direction: row;
        }

        :host([position="bottom"]) > #container {
            flex-direction: column-reverse;
        }
        :host([position="bottom"]) ::slotted(kj-tabber-head) {
            flex-direction: row;
        }

        :host([position="left"]) > #container {
            flex-direction: row;
        }
        :host([position="left"]) ::slotted(kj-tabber-head) {
            flex-direction: column;
        }

        :host([position="right"]) > #container {
            flex-direction: row-reverse;
        }
        :host([position="right"]) ::slotted(kj-tabber-head) {
            flex-direction: column;
        }
        `
    ]


    protected _elHead: TabberHead = null;
    protected _elBody: TabberBody = null;

    @property({ reflect: true }) position: "top" | "bottom" | "left" | "right" = "top";

    constructor() {
        super();
    }

    render() {
        return html`
        <div id="container">
            <slot @slotchange=${ this.__onSlotChange }></slot>
        </div>`
    }

    protected __onSlotChange(ev: Event) {
        console.log("__onSlotChange");

        this._elHead = null;
        this._elBody = null;

        const els = (<HTMLSlotElement>ev.target).assignedElements();
        for (let el of els) {
            if (el instanceof TabberHead && this._elHead === null) {           // берётся только первый элемент TabbedHead
                this._elHead = el

            } else if (el instanceof TabberBody && this._elBody === null) {    // берётся только первый элемент TabbedBody
                this._elBody = el

            } else {
                el.remove()
            }
        }

        if (this._elHead) {
            this._elHead.addEventListener("selected", () => {
                if (this._elBody) {
                    this._elBody.showPage(this._elHead.selectedTab.name)
                }
            })
        }
        if (this._elBody) {
            this._elBody.addEventListener("changeElements", () => {
                if (this._elHead && this._elHead.selectedTab) {
                    this._elBody.showPage(this._elHead.selectedTab.name)
                }
            });
        }
    }
}

declare global {
    interface HTMLElementTagNameMap {
        "kj-tabber": Tabber;
    }
}
