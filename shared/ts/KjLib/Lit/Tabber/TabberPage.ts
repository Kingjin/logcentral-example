import { LitElement, html, css } from 'lit';
import { customElement, property, state } from 'lit/decorators.js';


const ELEMENT_NAME = "kj-tabber-page"

@customElement(ELEMENT_NAME)
export default class TabberPage extends LitElement {

    static styles = [
        css`
        :host {
            position: relative;
            /*inset: 0;
            /*width: 100%;
            height: 100%;*/
            background: var(--kj-tabber-page--background, transparent);
        }
        `
    ]


    constructor() {
        super();
        //this.tabIndex = 0;
    }

    @property({ reflect: true }) name = "";

    render() {
        return html`<slot></slot>`
    }

    /*attributeChangedCallback(name: string, oldVal: string, newVal: string) {
        if (name === "selected" && newVal !== null) {
            this.dispatchEvent(new Event("selected"))
        }
        super.attributeChangedCallback(name, oldVal, newVal);
    }*/

}

declare global {
    interface HTMLElementTagNameMap {
        [ELEMENT_NAME]: TabberPage;
    }
}