import { LitElement, html, css } from 'lit';
import { customElement, property, state } from 'lit/decorators.js';

import TabberPage from "./TabberPage.js"

const ELEMENT_NAME = "kj-tabber-body"

@customElement(ELEMENT_NAME)
export default class TabberBody extends LitElement {

    static styles = [
        css`
        :host {
            position: relative;
            background: var(--kj-tabber-body--background, transparent);
        }`
    ];


    protected _mapPages: Map<string, TabberPage> = null;

    @state() protected _selectedPage: TabberPage = null;

    constructor() {
        super();
        this._mapPages = new Map();
    }

    render() {
        return html`<slot @slotchange=${ this.__onSlotChange }></slot>`
    }

    protected __onSlotChange(ev: Event) {
        console.log("__onSlotChange_body");

        this._mapPages.clear();
        this._selectedPage = null;

        const els = (<HTMLSlotElement>ev.target).assignedElements();
        for (let el of els) {
            if (el instanceof TabberPage) {
                const page: TabberPage = el;
                this._mapPages.set(page.name, this.__hidePage(page))
            } else {
                el.remove()
            }
        }

        this.dispatchEvent(new Event("changeElements"));
    }

    showPage(name: string) {
        if (this._selectedPage) {
            if (this._selectedPage.name === name) return;

            this.__hidePage(this._selectedPage);
        }

        this._selectedPage = this._mapPages.has(name) ? this.__showPage(this._mapPages.get(name)) : null;
    }


    protected __showPage(page: TabberPage): TabberPage {
        //page.style.zIndex  = "1";
        page.style.display = "block";
        page.style.opacity = "1";
        return page
    }

    protected __hidePage(page: TabberPage): TabberPage {
        //page.style.zIndex  = "-1";
        page.style.display = "none";
        page.style.opacity = "0";
        return page
    }


}

declare global {
    interface HTMLElementTagNameMap {
        [ELEMENT_NAME]: TabberBody;
    }
}