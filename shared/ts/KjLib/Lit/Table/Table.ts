import { LitElement, html, css, svg, TemplateResult, CSSResultGroup } from "lit";
import { customElement, property, state } from "lit/decorators.js";

import "./TableHead"
import "./TableBody"
import "./TableFoot"
import "./TableRow"
import "./TableCell"

import TableHead from "./TableHead"
import TableBody from "./TableBody"
import TableFoot from "./TableFoot"
import TableRow  from "./TableRow"
import TableCell from "./TableCell"

@customElement("kj-table")
export default class Table extends LitElement {

    protected _thead: TableHead = null;
    protected _tbody: TableBody = null;
    protected _tfoot: TableFoot = null;

    constructor() {
        super();
        this.__onSlotChange = this.__onSlotChange.bind(this);
        this.__headChanged = this.__headChanged.bind(this);
    }

    //@property({ type: String }) title = "";

    static styles = css`
    :host {
        display: grid;
        position: relative;
        z-index: 0;
    }
    #container {
        display: flex;
        position: relative;
        flex-direction: column;
        background: inherit;
    }
    #slot {
        display: flex;
        position: relative;
        flex-direction: column;
        z-index: 0;
        flex: 1;
    }`

    render() {
        return html`
        <div id="container">
            <slot @slotchange="${ this.__onSlotChange }"></slot>
        </div>`;
    }

    protected __onSlotChange(ev: Event) {
        this._thead = null;
        this._tbody = null;
        this._tfoot = null;

        const elems = (<HTMLSlotElement>ev.target).assignedElements();
        for (let el of elems) {
            if (el instanceof TableHead) {
                this._thead = el;
                el.addEventListener("changed", this.__headChanged)

            } else if (el instanceof TableBody) {
                this._tbody = el

            } else if (el instanceof TableFoot) {
                this._tfoot = el

            } else {
                el.remove()
            }
        }
        //* Dispatch items loaded event
        this.dispatchEvent(new Event("changed"));
    }

    protected __headChanged(ev: Event) {
        if (this._tbody) {
            this._tbody.updateRows()
        }
    }

    get tableHead(): TableHead {
        return this._thead
    }
}