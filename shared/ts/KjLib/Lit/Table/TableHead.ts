import { LitElement, html, css, svg, TemplateResult, CSSResultGroup } from "lit";
import { customElement, property, state } from "lit/decorators.js";

import TableColumn from "./TableColumn"

@customElement("kj-table-head")
export default class TableHead extends LitElement {

    static styles = css`
    :host {
        position: sticky;
        top: 0;
        z-index: 1;
    }
    #basis {
        background: var(--background, initial);
        /*background: inherit;*/
    }
    #cells {
        position: relative;
        display: flex;
        flex-direction: row;
        z-index: 0;
        /*background: inherit;*/
        /*background: var(--background, initial);*/
    }
    #left {
        position: sticky;
        left: 0;
        display: flex;
        flex-direction: row;
        z-index: 1;
        background: var(--background, initial);
        box-shadow: 2px 0px 2px 1px rgba(255,255,255,0.5);
    }
    #middle {
        position: relative;
        display: flex;
        flex-direction: row;
        align-items: stretch;
        z-index: 0;
        flex: 1;
    }
    #right {
        position: sticky;
        right: 0;
        display: flex;
        flex-direction: row;
        align-items: stretch;
        z-index: 1;
        background: var(--background, initial);
        box-shadow: -2px 0px 2px 1px rgba(255,255,255,0.5);
    }
    .cell {
        display: inherit;
        position: relative;
        box-sizing: border-box;
        margin: var(--cell-margin, none);
        /*border-radius: 2px;*/
        background: white;
        box-shadow: 0px 1px 3px 0px rgba(0,0,0,0.5);
        /*padding: 5px 10px;*/
        /*padding: var(--cell--padding, 8px 10px);*/
    }
    .cell:hover {
        box-shadow: 0px 1px 5px 1px rgba(255,165,0,0.5);
        /*background: gold;*/
    }`

    protected _tcolumns: TableColumn[] = [];

    private _allocColumn = false;

    constructor() {
        super();
        this.__onSortChange = this.__onSortChange.bind(this);
        this.__onSlotChange = this.__onSlotChange.bind(this);
    }

    render() {
        //! Ячейки и слоты для вставки <table-column>, добавляются автоматически
        return html`
        <div id="basis">
            <div id="cells">
                <slot @slotchange="${ this.__onSlotChange }"></slot>
                <div id="left"><!-- ячейки создаются динамически --></div>
                <div id="middle"><!-- ячейки создаются динамически --></div>
                <div id="right"><!-- ячейки создаются динамически --></div>
            </div>
        </div>`;
    }

    protected __onSortChange(ev: Event) {
        const tcol = <TableColumn>ev.currentTarget;

        for (let i = 0; i < this._tcolumns.length; i++) {
            if (this._tcolumns[i] === tcol) continue;

            if (this._tcolumns[i].sort !== null) {
                console.log(this._tcolumns[i].name);
                this._tcolumns[i].sort = ""
            }
        }
    }

    protected __onSlotChange(ev: Event) {
        console.log(ev);

        if (this._allocColumn) {
            this._allocColumn = false;
            return
        }

        const elLeft = <HTMLDivElement>this.shadowRoot.getElementById("left");
        const elRight = <HTMLDivElement>this.shadowRoot.getElementById("right");
        const elMiddle = <HTMLDivElement>this.shadowRoot.getElementById("middle");

        this._tcolumns.length = 0;

        const elements = (<HTMLSlotElement>ev.target).assignedElements();
        for (let el of elements) {
            if (el instanceof TableColumn) {
                this._tcolumns.push(el)
            } else {
                el.remove()
            }
        }

        if (this._tcolumns.length > 0) {
            this._allocColumn = true
        }

        // Для каждого <table-column> создаётся ячейка <div class="cell">
        // и в ней слот <slot name="columnName">, куда добавляется <table-column>
        for (let i = 0, len = this._tcolumns.length; i < len; i++) {

            const tcol = this._tcolumns[i];
            tcol.addEventListener("sortchange", this.__onSortChange);

            const columnName = tcol.getAttribute("name");
            tcol.slot = columnName;

            const cell = document.createElement("div");
            cell.className = "cell";

            const width = tcol.getAttribute("width");
            if (width) {
                cell.style.width = width;
                cell.style.minWidth = width;
                cell.style.maxWidth = width;
            } else {
                cell.style.flex = "1";
            }

            const slot = document.createElement("slot");
            slot.name = columnName;

            cell.appendChild(slot);

            if (tcol.getAttribute("fixed-left") !== null) {
                elLeft.appendChild(cell)
            } else if (tcol.getAttribute("fixed-right") !== null) {
                elRight.appendChild(cell)
            } else {
                elMiddle.appendChild(cell)
            }
        }

        this.dispatchEvent(new CustomEvent("changed", { detail: "middle" }));
    }

    /** Массив столбцов */
    get tableColumns(): TableColumn[] {
        return this._tcolumns
    }
}