import { LitElement, html, css, svg, TemplateResult, CSSResultGroup } from "lit";
import { customElement, property, state } from "lit/decorators.js";

function sortNone() {
    return svg`
    <svg width="12" height="12" viewBox="0 0 24 24">
        <path d="M12 0l8 10h-16l8-10zm8 14h-16l8 10 8-10z"/>
    </svg>`
}
// <path d="M8 10v4h4l-6 7-6-7h4v-4h-4l6-7 6 7h-4zm16 5h-10v2h10v-2zm0 6h-10v-2h10v2zm0-8h-10v-2h10v2zm0-4h-10v-2h10v2zm0-4h-10v-2h10v2z"/>
function sortAsc() {
    return svg`
    <svg width="16" height="16" viewBox="0 0 24 24">
        <path d="M6 21l6-8h-4v-10h-4v10h-4l6 8zm16-4h-8v-2h8v2zm2 2h-10v2h10v-2zm-4-8h-6v2h6v-2zm-2-4h-4v2h4v-2zm-2-4h-2v2h2v-2z"/>
    </svg>`
}

function sortDesc() {
    return svg`
    <svg width="16" height="16" viewBox="0 0 24 24">
        <path d="M6 21l6-8h-4v-10h-4v10h-4l6 8zm16-12h-8v-2h8v2zm2-6h-10v2h10v-2zm-4 8h-6v2h6v-2zm-2 4h-4v2h4v-2zm-2 4h-2v2h2v-2z"/>
    </svg>`
}

@customElement("kj-table-column")
export default class TableColumn extends LitElement {

    static styles = css`
    :host {
        position: relative;
        box-sizing: border-box;
        width: 100%;
        /*margin: 2px;
        /*margin: var(--cell-margin, none);
        /*padding: 5px 10px;*/
    }
    :host(:hover) {
        cursor: pointer;
    }
    #container {
        display: flex;
        position: relative;
        box-sizing: border-box;
        width: 100%;
        /*padding: 5px 10px;*/
    }
    #title {
        flex: 1;
    }`


    @property({ type: String, reflect: true })
    name = "";

    @property({ type: String, reflect: true })
    width = "";

    @property({ type: String, reflect: true, attribute: "min-width" })
    minWidth = "";

    @property({ type: String, reflect: true })
    sort: null | "" | "asc" | "desc" = null;

    constructor() {
        super();
        this.__onClick = this.__onClick.bind(this);
    }

    protected __sortIcon(sort: string): TemplateResult {
        if (sort === "") {
            return sortNone()
        }
        if (sort === "asc") {
            return sortAsc()
        }
        if (sort === "desc") {
            return sortDesc()
        }
        return null
    }

    render() {
        return html`
        <div id="container">
            <div id="title">
                <slot></slot>
            </div>
            <div>${ this.__sortIcon(this.sort) }</div>
        </div>
        `
    }

    protected __onClick(ev: Event) {
        if (this.sort === null) return;

        if (this.sort === "") {
            this.sort = "asc"
        } else if (this.sort === "asc") {
            this.sort = "desc";
        } else {
            this.sort = "asc";
        }
    }

    attributeChangedCallback(name, oldVal, newVal) {
        if (name === "sort") {
            if (newVal === "asc" || newVal === "desc") {
                this.dispatchEvent(new Event("sortchange"))
            }
        }
        super.attributeChangedCallback(name, oldVal, newVal);
      }

    firstUpdated(changedProperties) {
        this.addEventListener("click", this.__onClick);
    }
}