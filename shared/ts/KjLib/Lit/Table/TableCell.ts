import { LitElement, html, css, svg, TemplateResult, CSSResultGroup } from "lit";
import { customElement, property, state } from "lit/decorators.js";


@customElement("kj-table-cell")
export default class TableCell extends LitElement {

    static styles = css`
    slot {
        position: relative;
        z-index: 0;
    }`

    constructor() {
        super();
        this.__onSlotChange = this.__onSlotChange.bind(this);
    }

    render() {
        return html`<slot @slotchange="${ this.__onSlotChange }"></slot>`;
    }

    protected __onSlotChange() {
        this.dispatchEvent(new CustomEvent("cellChanged", { bubbles: true, detail: { row: null, cell: this } }))
    }
}