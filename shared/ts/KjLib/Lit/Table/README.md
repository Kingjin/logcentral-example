# Table

## Описание

**Table** - таблица со sticky-фиксированными заголовком и крайними столбцами.

## HTML syntax
```html
<kj-table>
    <kj-table-head>
        <kj-table-column></kj-table-column>
        ...
    </kj-table-head>
    <kj-table-body>
        <kj-table-row>
            <kj-table-cell></kj-table-cell>
            ...
        </kj-table-row>
        ...
    </kj-table-body>
</kj-table>
```
## Example

```html
<kj-table id="table">
    <kj-table-head>
        <kj-table-column sort name="id" width="80px" fixed-left>ID</kj-table-column>
        <kj-table-column sort="asc" name="col1" width="150px">Column 1</kj-table-column>
        <kj-table-column sort="desc" name="col2" width="150px">Column 2</kj-table-column>
        <kj-table-column name="col3" width="150px">Column 3</kj-table-column>
        <kj-table-column name="col4" width="150px">Column 4</kj-table-column>
        <kj-table-column name="col5" width="150px">Column 5</kj-table-column>
        <kj-table-column name="col6" width="150px">Column 6</kj-table-column>
        <kj-table-column name="-"></kj-table-column>
        <kj-table-column name="actions" width="120px" fixed-right>Actions</kj-table-column>
    </kj-table-head>
    <kj-table-body id="table_body">
        <kj-table-row>
            <kj-table-cell slot="id">1</kj-table-cell>
            <kj-table-cell slot="col1">row 1 col 1</kj-table-cell>
            <kj-table-cell slot="col2">row 1 col 2</kj-table-cell>
            <kj-table-cell slot="col3">row 1 col 3</kj-table-cell>
            <kj-table-cell slot="col4">row 1 col 4</kj-table-cell>
            <kj-table-cell slot="col5">row 1 col 5</kj-table-cell>
            <kj-table-cell slot="col6">row 1 col 6</kj-table-cell>
        </kj-table-row>
        <kj-table-row>
            <kj-table-cell slot="id">2</kj-table-cell>
            <kj-table-cell slot="col1">row 2 col 1</kj-table-cell>
            <kj-table-cell slot="col2">row 2 col 2</kj-table-cell>
            <kj-table-cell slot="col3">row 2 col 3</kj-table-cell>
            <kj-table-cell slot="col4">row 2 col 4</kj-table-cell>
            <kj-table-cell slot="col5">row 2 col 5</kj-table-cell>
            <kj-table-cell slot="col6">row 2 col 6</kj-table-cell>
        </kj-table-row>
        <kj-table-row>
            <kj-table-cell slot="id">3</kj-table-cell>
            <kj-table-cell slot="col1">row 3 col 1</kj-table-cell>
            <kj-table-cell slot="col2">row 3 col 2</kj-table-cell>
            <kj-table-cell slot="col3">row 3 col 3</kj-table-cell>
            <kj-table-cell slot="col4">row 3 col 4</kj-table-cell>
            <kj-table-cell slot="col5">row 3 col 5</kj-table-cell>
            <kj-table-cell slot="col6">row 3 col 6</kj-table-cell>
        </kj-table-row>
    </kj-table-body>
</kj-table>
```