import { LitElement, html, css, svg, TemplateResult, CSSResultGroup } from "lit";
import { customElement, property, state } from "lit/decorators.js";

import Table from "./Table"
import TableHead from "./TableHead"
import TableBody from "./TableBody"
import TableCell from "./TableCell"

@customElement("kj-table-row")
export default class TableRow extends LitElement {

    static styles = css`
    :host {

    }
    :host([selected]) {
        /*background: var(--background-selected, #32cd32);*/
    }
    #slot {
        position: relative;
        display: flex;
        flex-direction: row;
        z-index: 0;
    }
    #slotDetail {
        position: relative;
        z-index: 0;
    }
    #row {
        cursor: default;
    }
    #cells {
        position: relative;
        display: flex;
        flex-direction: row;
        z-index: 0;
        background: var(--background, initial);
        /*border-bottom: 1px solid var(--border-color, #777);
        background: var(--background, #FFFFFF);
        transition: background-color 250ms ease;*/
    }
    #fixedLeft {
        position: sticky;
        left: 0;
        display: flex;
        flex-direction: row;
        align-items: stretch;
        z-index: 1;
        background: var(--background, initial);
        box-shadow: 2px 0px 2px 1px rgba(255,255,255,0.5);
    }
    #fixedLeft .cell {
        /*border-right: 1px solid var(--border-color, #777);*/
    }
    #middle {
        position: relative;
        display: flex;
        flex-direction: row;
        align-items: stretch;
        z-index: 0;
        flex: 1;
    }
    #fixedRight {
        position: sticky;
        right: 0;
        display: flex;
        flex-direction: row;
        align-items: stretch;
        z-index: 1;
        /*background: var(--background, #FFFFFF);
        transition: background-color 250ms ease;*/
        background: var(--background, initial);
        box-shadow: -2px 0px 2px 1px rgba(255,255,255,0.5);
    }
    .cell {
        display: inherit;
        position: relative;
        box-sizing: border-box;
        margin: var(--cell-margin, none);
        /*border-radius: 2px;*/
        background: white;
        box-shadow: 0px 1px 3px 0px rgba(0,0,0,0.5);
        /*padding: 5px 10px;*/
        /*padding: var(--cell--padding, 8px 10px);*/
        transition: background 250ms ease;
        overflow: hidden;
    }
    .cell:hover {
        /*box-shadow: 0px 1px 5px 1px rgba(255,165,0,0.5);*/
        background: #FFFF80;
    }
    .hover {
        /*box-shadow: 0px 1px 5px 1px rgba(255,165,0,0.5);*/
    }
    .hl {
        /*box-shadow: 0px 1px 5px 1px rgba(255,0,0,0.5);*/
        background: #FFFFC0;
    }
    #detail {
        transition: height 500ms ease;
        overflow: hidden;
        /*background: var(--background, #FFFFFF);
        background-color: #6b98a7;*/
    }
    `

    protected _elLeft: HTMLDivElement;
    protected _elMiddle: HTMLDivElement;
    protected _elFixedRight: HTMLDivElement;
    protected _elFixedLeft: HTMLDivElement;
    protected _elToggleDetail: HTMLDivElement;
    protected _elDetail: HTMLDivElement;

    protected _tcells: TableCell[] = [];

    protected _elCells: HTMLDivElement[] = [];

    protected _mapShadeCells: Map<string, HTMLDivElement> = new Map();

    constructor() {
        super();
    }

    render() {
        return html`
        <div id="row">
            <div id="cells">
                <div id="fixedLeft"><!-- ячейки создаются динамически --></div>
                <div id="middle"><!-- ячейки создаются динамически --></div>
                <div id="fixedRight"><!-- ячейки создаются динамически --></div>
            </div>
            <div id="detail" style="height: 0">
                <slot id="slotDetail" name="detail"></slot>
            </div>
        </div>
        `;
    }

    updateCells() {
        this.__updateCells();
    }

    hlCell(columnName: string) {
        if (this._mapShadeCells.has(columnName)) {
            this._mapShadeCells.get(columnName).classList.add("hl");
        }
    }

    hlCellClear(columnName: string) {
        if (this._mapShadeCells.has(columnName)) {
            this._mapShadeCells.get(columnName).classList.remove("hl");
        }
    }

    protected __updateCells() {
        this._mapShadeCells.clear();

        this._elMiddle.innerHTML = "";
        this._elFixedLeft.innerHTML = "";
        this._elFixedRight.innerHTML = "";

        const elements = this.parentElement.parentElement.getElementsByTagName("kj-table-head")[0].children;
        for (let el of elements) {
            const columnName = el.getAttribute("name");

            const cell = document.createElement("div");
            cell.className = "cell";
            cell.onmouseover = () => {
                for (let v of this._mapShadeCells.values()) {
                    v.classList.add("hl")
                }
                this.dispatchEvent(new CustomEvent("cellMouseOver", { detail: { columnName } }))
            }
            cell.onmouseout = () => {
                for (let v of this._mapShadeCells.values()) {
                    v.classList.remove("hl")
                }
                this.dispatchEvent(new CustomEvent("cellMouseOut", { detail: { columnName } }))
            }

            const width = el.getAttribute("width");
            if (width) {
                cell.style.width = width;
                cell.style.minWidth = width;
                cell.style.maxWidth = width;
            } else {
                cell.style.flex = "1";
            }

            const slot = document.createElement("slot");
            slot.name = columnName;
            slot.addEventListener("slotchange", () => {
                const elems = slot.assignedElements();
                if (!(elems.length === 1 && elems[0] instanceof TableCell)) {
                    for (let el of elems) {
                        el.remove()
                    }
                }
            });

            cell.appendChild(slot);

            if (el.getAttribute("fixed-left") !== null) {
                this._elFixedLeft.appendChild(cell)
            } else if (el.getAttribute("fixed-right") !== null) {
                this._elFixedRight.appendChild(cell)
            } else {
                this._elMiddle.appendChild(cell)
            }

            this._elCells.push(cell);
            this._mapShadeCells.set(columnName, cell);
        }
    }

    firstUpdated(changedProperties) {
        this._elMiddle = <HTMLDivElement>this.shadowRoot.getElementById("middle");
        this._elFixedLeft = <HTMLDivElement>this.shadowRoot.getElementById("fixedLeft");
        this._elFixedRight = <HTMLDivElement>this.shadowRoot.getElementById("fixedRight");

        this.__updateCells();
    }

}