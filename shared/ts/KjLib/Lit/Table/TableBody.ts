import { LitElement, html, css, svg, TemplateResult, CSSResultGroup } from "lit";
import { customElement, property, state } from "lit/decorators.js";

import TableRow from "./TableRow";
import TableCell from "./TableCell";

@customElement("kj-table-body")
export default class TableBody extends LitElement {

    protected _trows: TableRow[] = [];

    constructor() {
        super();
        this.__onSlotChange = this.__onSlotChange.bind(this);
    }

    static styles = css`
    :host {
        flex: 1;
    }
    #slot {
        position: relative;
        display: flex;
        flex-direction: column;
        z-index: 0;
    }`


    render() {
        return html`<slot id="slot" @slotchange="${ this.__onSlotChange }"></slot>`;
    }

    protected __onSlotChange(ev: Event) {
        this._trows.length = 0;

        const elements = (<HTMLSlotElement>ev.target).assignedElements();
        for (let el of elements) {
            if (!(el instanceof TableRow)) {
                el.remove()
            } else {
                this._trows.push(el);
                el.addEventListener("cellMouseOver", ev => {
                    const columnName = (<CustomEvent>ev).detail.columnName
                    for (let trow of this._trows) {
                        trow.hlCell(columnName);
                    }
                });
                el.addEventListener("cellMouseOut", ev => {
                    const columnName = (<CustomEvent>ev).detail.columnName
                    for (let trow of this._trows) {
                        trow.hlCellClear(columnName);
                    }
                });
            }
        }
        this.dispatchEvent(new CustomEvent("changed"));
    }

    updateRows() {
        for (let trow of this._trows) {
            console.log("TableBody:Row -> updateCells");
            trow.updateCells()
        }
    }
}