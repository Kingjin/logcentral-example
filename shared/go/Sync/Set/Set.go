package Sync

import "sync"

type Set[T comparable] struct {
	mu sync.RWMutex
	m  map[T]struct{}
}

func NewSet[T comparable]() *Set[T] {
	return &Set[T]{
		m: make(map[T]struct{}),
	}
}

func (s *Set[T]) Add(item T) {
	s.mu.Lock()
	defer s.mu.Unlock()
	s.m[item] = struct{}{}
}

func (s *Set[T]) Remove(item T) {
	s.mu.Lock()
	defer s.mu.Unlock()
	delete(s.m, item)
}

func (s *Set[T]) Has(item T) bool {
	s.mu.RLock()
	defer s.mu.RUnlock()
	_, ok := s.m[item]
	return ok
}

func (s *Set[T]) Len() int {
	s.mu.RLock()
	defer s.mu.RUnlock()
	return len(s.m)
}

func (s *Set[T]) ForEach(f func(T) error) (err error) {
	s.mu.RLock()
	defer s.mu.RUnlock()
	for v := range s.m {
		if err = f(v); err != nil {
			break
		}
	}
	return
}
