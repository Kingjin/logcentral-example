package Sync

import "sync"

type Map[K comparable, V any] struct {
	mu sync.RWMutex
	m  map[K]V
}

func NewMap[K comparable, V any]() *Map[K, V] {
	return &Map[K, V]{
		m: make(map[K]V),
	}
}

func (s *Map[K, V]) Add(k K, v V) {
	s.mu.Lock()
	defer s.mu.Unlock()
	s.m[k] = v
}

func (s *Map[K, V]) Remove(k K) {
	s.mu.Lock()
	defer s.mu.Unlock()
	delete(s.m, k)
}

func (s *Map[K, V]) Has(k K) bool {
	s.mu.RLock()
	defer s.mu.RUnlock()
	_, ok := s.m[k]
	return ok
}

func (s *Map[K, V]) Len() int {
	s.mu.RLock()
	defer s.mu.RUnlock()
	return len(s.m)
}

func (s *Map[K, V]) ForEach(f func(K, V) error) (err error) {
	s.mu.RLock()
	defer s.mu.RUnlock()
	for k, v := range s.m {
		if err = f(k, v); err != nil {
			break
		}
	}
	return
}
