package ConsoleLogger

import (
	"fmt"
	"log"
	"runtime"

	"github.com/TwiN/go-color"
)

type Logger struct {
}

func NewLogger() *Logger {
	//log.SetFlags(log.LstdFlags | log.Lshortfile)
	return &Logger{}
}

func (l Logger) sprint(prefix string, v ...any) string {
	pc /*file*/, _, line, ok := runtime.Caller(3)
	if !ok {
		//file = "?"
		line = 0
	}
	// fmt.Sprint(prefix, " ", runtime.FuncForPC(pc).Name(), " ", file, ":", line, "\n  >> ") + fmt.Sprint(v...)
	return fmt.Sprint(prefix, " ", runtime.FuncForPC(pc).Name(), ":", line, "\n  >> ") + fmt.Sprint(v...)
}

func (l Logger) Info(v ...any) {
	log.Println(color.InBlue(l.sprint("[I]", v...)))
}

func (l Logger) Warn(v ...any) {
	log.Println(color.InYellow(l.sprint("[W]", v...)))
}

func (l Logger) Error(v ...any) {
	log.Println(color.InRed(l.sprint("[E]", v...)))
}
