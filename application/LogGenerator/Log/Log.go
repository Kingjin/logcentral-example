package Log

import (
	"Logger"
)

type LogFunc func(v ...any)

var Info LogFunc
var Warn LogFunc
var Error LogFunc

func init() {
	Info = func(v ...any) {}
	Warn = func(v ...any) {}
	Error = func(v ...any) {}
}

func SetLoggers(loggers ...Logger.ILogger) {
	Info = func(v ...any) {
		for _, logger := range loggers {
			logger.Info(v...)
		}
	}

	Warn = func(v ...any) {
		for _, logger := range loggers {
			logger.Warn(v...)
		}
	}

	Error = func(v ...any) {
		for _, logger := range loggers {
			logger.Error(v...)
		}
	}
}
