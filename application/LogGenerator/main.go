package main

import (
	"LogGenerator/Config"
	"LogGenerator/ConsoleLogger"
	"LogGenerator/Gens"
	"LogGenerator/Log"
	"context"
	"fmt"
	"log"
	"math/rand"
	"os/signal"
	"strings"
	"syscall"
	"time"

	"CentralLogger"
	"LogProducerGRPCClientAdapter"
)

func main() {
	conf, err := Config.NewConfig("config.yaml")
	if err != nil {
		log.Fatal(err)
		return
	}

	ctx, stop := signal.NotifyContext(context.Background(), syscall.SIGINT, syscall.SIGTERM)
	defer stop()

	clientProducer := LogProducerGRPCClientAdapter.NewLogProducerClient(
		conf.Producer.Name,
		conf.Producer.Target)

	// Создание 3-х байтовой метки запуска (минуты, секунды, миллисекунды)
	// для идентификации запущенного экземпляра приложения - устанавливается в log.src
	t := time.Now()
	tm := uint32((t.Minute() << 18) | (t.Second() << 12) | t.Nanosecond()/1000000)

	consoleLogger := ConsoleLogger.NewLogger()
	centralLogger := CentralLogger.NewLogger(clientProducer, "LogGen-"+strings.ToUpper(fmt.Sprintf("%x", tm)))

	go func() {
		for {
			err := centralLogger.Run(ctx)
			if err != nil {
				log.Println(err)
				time.Sleep(time.Second)
				continue
			}
			stop()
			break
		}
	}()

	Log.SetLoggers(consoleLogger, centralLogger)

	rand.Seed(time.Now().UnixNano())

	go Gens.RunGenLogInfo(ctx, 5000)
	go Gens.RunGenLogWarn(ctx, 10000)
	go Gens.RunGenLogError(ctx, 20000)

	<-ctx.Done()

	fmt.Println("shutting down gracefully, press Ctrl+C again to force")
	time.Sleep(3 * time.Second)
	fmt.Println("LogGenerator is finished")
}
