module LogGenerator

go 1.18

require (
	CentralLogger v0.0.0
	gopkg.in/yaml.v2 v2.4.0
)

require (
	LogProducerGRPCClientAdapter v0.0.0
	Logger v0.0.0
	github.com/TwiN/go-color v1.4.0
)

require (
	LogMessage v0.0.0 // indirect
	LogProducerGRPCClient v0.0.0 // indirect
	PbLogMessageConverter v0.0.0 // indirect
	github.com/golang/protobuf v1.5.2 // indirect
	golang.org/x/net v0.5.0 // indirect
	golang.org/x/sys v0.4.0 // indirect
	golang.org/x/text v0.6.0 // indirect
	google.golang.org/genproto v0.0.0-20230110181048-76db0878b65f // indirect
	google.golang.org/grpc v1.53.0 // indirect
	google.golang.org/protobuf v1.28.1 // indirect
	proto v0.0.0 // indirect
)

replace (
	CentralLogger v0.0.0 => ../../infrastructure/CentralLogger
	LogMessage v0.0.0 => ../../infrastructure/logging/LogMessage
	LogProducerGRPCClient v0.0.0 => ../../infrastructure/grpc/LogProducerGRPCClient
	LogProducerGRPCClientAdapter v0.0.0 => ../../infrastructure/adapters/LogProducerGRPCClientAdapter
	Logger v0.0.0 => ../../infrastructure/logging/Logger
	PbLogMessageConverter v0.0.0 => ../../infrastructure/adapters/PbLogMessageConverter
	Sync v0.0.0 => ../../shared/go/Sync
	proto v0.0.0 => ../../infrastructure/grpc/proto
)
