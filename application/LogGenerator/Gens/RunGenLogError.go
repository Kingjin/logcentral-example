package Gens

import (
	"LogGenerator/Log"
	"context"
	"fmt"
	"math/rand"
	"time"
)

func RunGenLogError(ctx context.Context, ms int64) {
	fn := getFuncName()
	fmt.Println(fn, "is running")
	defer fmt.Println(fn, "is stopped")

	var messages = []string{
		"Database connection error",
		"Internal server error",
		"Product not found",
		"Invalid authentication token",
		"Invalid request format",
		"Failed to process payment",
		"User account blocked",
		"Unauthorized access attempt",
		"Insufficient funds",
		"Critical system error",
	}

	for {
		select {
		case <-ctx.Done():
			return
		case <-time.After(time.Duration(rand.Int63n(ms)) * time.Millisecond):
			Log.Error(messages[rand.Intn(len(messages))])
		}
	}
}
