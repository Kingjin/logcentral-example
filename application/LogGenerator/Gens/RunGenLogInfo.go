package Gens

import (
	"LogGenerator/Log"
	"context"
	"fmt"
	"math/rand"
	"time"
)

func RunGenLogInfo(ctx context.Context, ms int64) {
	fn := getFuncName()
	fmt.Println(fn, "is running")
	defer fmt.Println(fn, "is stopped")

	var messages = []string{
		"User signed up successfully",
		"Product added to cart",
		"Payment processed successfully",
		"New user registered",
		"Order placed successfully",
		"User profile updated",
		"Product review added",
		"New promotion created",
		"Invoice sent to customer",
		"Order shipped",
	}

	for {
		select {
		case <-ctx.Done():
			return
		case <-time.After(time.Duration(rand.Int63n(ms)) * time.Millisecond):
			Log.Info(messages[rand.Intn(len(messages))])
		}
	}
}
