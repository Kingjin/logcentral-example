package Gens

import (
	"LogGenerator/Log"
	"context"
	"fmt"
	"math/rand"
	"time"
)

func RunGenLogWarn(ctx context.Context, ms int64) {
	fn := getFuncName()
	fmt.Println(fn, "is running")
	defer fmt.Println(fn, "is stopped")

	var messages = []string{
		"User session expired",
		"Product inventory running low",
		"Invalid input data received",
		"Payment authorization failed",
		"Duplicate order detected",
		"Incorrect login credentials",
		"Unrecognized product category",
		"Email delivery failed",
		"Payment declined by bank",
		"User account temporarily suspended",
	}

	for {
		select {
		case <-ctx.Done():
			return
		case <-time.After(time.Duration(rand.Int63n(ms)) * time.Millisecond):
			Log.Warn(messages[rand.Intn(len(messages))])
		}
	}
}
