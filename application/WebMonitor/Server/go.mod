module WebMonitor

go 1.18

require (
	LogConsumerGRPCClientAdapter v0.0.0
	LogMessage v0.0.0
	Sync v0.0.0
	github.com/gin-contrib/sessions v0.0.5
	github.com/gin-gonic/gin v1.9.0
	github.com/gorilla/websocket v1.5.0
	gopkg.in/yaml.v2 v2.2.8

)

require (
	LogConsumerGRPCClient v0.0.0 // indirect
	PbLogMessageConverter v0.0.0 // indirect
	github.com/bytedance/sonic v1.8.0 // indirect
	github.com/chenzhuoyu/base64x v0.0.0-20221115062448-fe3a3abad311 // indirect
	github.com/gin-contrib/sse v0.1.0 // indirect
	github.com/go-playground/locales v0.14.1 // indirect
	github.com/go-playground/universal-translator v0.18.1 // indirect
	github.com/go-playground/validator/v10 v10.11.2 // indirect
	github.com/goccy/go-json v0.10.0 // indirect
	github.com/golang/protobuf v1.5.2 // indirect
	github.com/gorilla/context v1.1.1 // indirect
	github.com/gorilla/securecookie v1.1.1 // indirect
	github.com/gorilla/sessions v1.2.1 // indirect
	github.com/json-iterator/go v1.1.12 // indirect
	github.com/klauspost/cpuid/v2 v2.0.9 // indirect
	github.com/leodido/go-urn v1.2.1 // indirect
	github.com/mattn/go-isatty v0.0.17 // indirect
	github.com/modern-go/concurrent v0.0.0-20180228061459-e0a39a4cb421 // indirect
	github.com/modern-go/reflect2 v1.0.2 // indirect
	github.com/pelletier/go-toml/v2 v2.0.6 // indirect
	github.com/quasoft/memstore v0.0.0-20191010062613-2bce066d2b0b // indirect
	github.com/twitchyliquid64/golang-asm v0.15.1 // indirect
	github.com/ugorji/go/codec v1.2.9 // indirect
	golang.org/x/arch v0.0.0-20210923205945-b76863e36670 // indirect
	golang.org/x/crypto v0.5.0 // indirect
	golang.org/x/net v0.7.0 // indirect
	golang.org/x/sys v0.5.0 // indirect
	golang.org/x/text v0.7.0 // indirect
	google.golang.org/genproto v0.0.0-20230110181048-76db0878b65f // indirect
	google.golang.org/grpc v1.53.0 // indirect
	google.golang.org/protobuf v1.28.1 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
	proto v0.0.0 // indirect
)

replace (
	LogConsumerGRPCClient v0.0.0 => ../../../infrastructure/grpc/LogConsumerGRPCClient
	LogConsumerGRPCClientAdapter v0.0.0 => ../../../infrastructure/adapters/LogConsumerGRPCClientAdapter
	LogMessage v0.0.0 => ../../../infrastructure/logging/LogMessage
	PbLogMessageConverter v0.0.0 => ../../../infrastructure/adapters/PbLogMessageConverter
	Sync v0.0.0 => ../../../shared/go/Sync
	proto v0.0.0 => ../../../infrastructure/grpc/proto
)
