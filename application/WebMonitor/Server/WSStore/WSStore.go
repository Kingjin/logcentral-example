package WSStore

import (
	Sync "Sync/Map"

	"github.com/gorilla/websocket"
)

type WSStoreKey = *websocket.Conn
type WSStoreValue = chan []byte

type WSStore struct {
	s *Sync.Map[WSStoreKey, WSStoreValue]
}

func NewWSStore() *WSStore {
	return &WSStore{
		s: Sync.NewMap[WSStoreKey, WSStoreValue](),
	}
}

func (ss *WSStore) Add(k WSStoreKey, v WSStoreValue) {
	ss.s.Add(k, v)
}

func (ss *WSStore) Remove(k WSStoreKey) {
	ss.s.Remove(k)
}

func (ss *WSStore) ForEach(f func(WSStoreKey, WSStoreValue) error) error {
	return ss.s.ForEach(f)
}
