package Config

import (
	"os"

	"gopkg.in/yaml.v2"
)

type Config struct {
	Consumer struct {
		Name   string `yaml:"name"`
		Target string `yaml:"target"`
	} `yaml:"consumer"`
	Web struct {
		Address string `yaml:"address"`
	} `yaml:"web"`
}

func NewConfig(path string) (*Config, error) {
	f, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	defer f.Close()

	config := &Config{}
	err = yaml.NewDecoder(f).Decode(config)
	if err != nil {
		return nil, err
	}
	return config, nil
}
