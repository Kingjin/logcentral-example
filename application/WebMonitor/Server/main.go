package main

import (
	"LogMessage"
	"WebMonitor/Config"
	"WebMonitor/Interfaces"
	"WebMonitor/WebServer"
	"context"
	"fmt"
	"log"
	"os/signal"
	"sync"
	"syscall"
	"time"

	"LogConsumerGRPCClientAdapter"
)

func main() {
	conf, err := Config.NewConfig("config.yaml")
	if err != nil {
		log.Fatal(err)
		return
	}

	ctx, stop := signal.NotifyContext(context.Background(), syscall.SIGINT, syscall.SIGTERM)
	defer stop()

	srvWeb := WebServer.NewServer()

	wg := &sync.WaitGroup{}
	wg.Add(1)
	go func() {
		defer wg.Done()
		err := srvWeb.Run(ctx, conf.Web.Address)
		if err != nil {
			fmt.Println(err)
			stop()
		}
	}()

	clientConsumer := Interfaces.ILogConsumerClient(LogConsumerGRPCClientAdapter.NewLogConsumerClient("ConsumerClient", conf.Consumer.Target))
	clientConsumer.SetReceiveHandler(onReceiveMessage(srvWeb))

	wg.Add(1)
	go func() {
		defer wg.Done()
		for {
			err := clientConsumer.Run(ctx)
			if err != nil {
				fmt.Println("ClientConsumer connection error:\n", err, "\nreconnect after 1 second..")
				time.Sleep(time.Second)
				continue
			}
			fmt.Println("ClientConsumer is stopped")
			stop()
			break
		}
	}()

	wg.Wait()

	fmt.Println("shutting down gracefully, press Ctrl+C again to force")
	time.Sleep(1 * time.Second)
	fmt.Println("WebMonitor is finished")
}

// onReceiveMessage вызывается при получении сообщения от Отправителя.
func onReceiveMessage(s *WebServer.Server) func(message LogMessage.LogMessage) {
	return func(message LogMessage.LogMessage) {
		err := s.SendToAllWS(message)
		if err != nil {
			log.Println(err)
		}
	}
}
