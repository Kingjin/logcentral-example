package Interfaces

import (
	"LogMessage"
	"context"
)

type ILogConsumerClient interface {
	SetReceiveHandler(func(LogMessage.LogMessage))
	GetReceiveHandler() func(LogMessage.LogMessage)
	Run(context.Context) error
}
