package Interfaces

import "github.com/gorilla/websocket"

type IWSStore interface {
	Add(*websocket.Conn, chan []byte)
	Remove(*websocket.Conn)
	ForEach(f func(*websocket.Conn, chan []byte) error) error
}
