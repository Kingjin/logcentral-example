package WebServer

import (
	"LogMessage"
	"WebMonitor/WSStore"
	"WebMonitor/WebServer/Routes"
	"context"
	"encoding/json"
	"fmt"

	"net/http"
	"time"

	"github.com/gin-contrib/sessions"
	"github.com/gin-contrib/sessions/memstore"
	"github.com/gin-gonic/gin"
	"github.com/gorilla/websocket"
)

type Server struct {
	wsStore *WSStore.WSStore
}

func NewServer() *Server {
	return &Server{}
}

func (s *Server) Run(ctx context.Context, addr string) error {
	fmt.Println("WebServer is running")
	defer fmt.Println("WebServer is stopped")

	s.wsStore = WSStore.NewWSStore()
	wsUpgrader := websocket.Upgrader{
		CheckOrigin: func(r *http.Request) bool {
			return true
		},
	}

	r := gin.Default()
	r.Static("/public", "../Client/build/public")
	//r.StaticFile("/favicon.png", "../Client/build/favicon.png")

	r.LoadHTMLFiles("../Client/build/index.html")

	store := memstore.NewStore([]byte("secret"))
	r.Use(sessions.Sessions("app", store))

	r.GET("/", Routes.Root)
	r.GET("/ws", Routes.WS(&wsUpgrader, s.wsStore))

	srv := &http.Server{
		Addr:    addr,
		Handler: r,
	}

	chnErr := make(chan error)
	defer close(chnErr)

	go func() {
		err := srv.ListenAndServe()
		if err == http.ErrServerClosed {
			return
		}
		chnErr <- err
	}()

	select {
	case err := <-chnErr:
		return err
	case <-ctx.Done():
		ctxCancel, cancel := context.WithTimeout(context.Background(), 5*time.Second)
		defer cancel()

		return srv.Shutdown(ctxCancel)
	}
}

func (s *Server) SendToAllWS(message LogMessage.LogMessage) error {
	dtoLog := struct {
		Src     string `json:"src"`
		Time    string `json:"time"`
		Level   int32  `json:"level"`
		Message string `json:"message"`
	}{
		Src:     message.Src,
		Time:    message.Time.Format(time.RFC3339),
		Level:   int32(message.Level),
		Message: message.Message,
	}

	jsonMessage, _ := json.Marshal(dtoLog)
	//log.Println("<<", string(jsonMessage))

	return s.wsStore.ForEach(func(conn *websocket.Conn, chnSend chan []byte) error {
		select {
		case chnSend <- jsonMessage:
			return nil
			// default:
			// 	return errors.New(conn.LocalAddr().String(), "not ready for sending")
		}
	})
}
