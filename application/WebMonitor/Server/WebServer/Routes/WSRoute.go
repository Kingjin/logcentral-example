package Routes

import (
	"WebMonitor/Interfaces"
	"log"

	"github.com/gin-gonic/gin"
	"github.com/gorilla/websocket"
)

// WS - "/ws"
func WS(wsUpgrader *websocket.Upgrader, wsStore Interfaces.IWSStore) gin.HandlerFunc {

	return func(c *gin.Context) {

		conn, err := wsUpgrader.Upgrade(c.Writer, c.Request, nil)
		if err != nil {
			log.Println("failed to set websocket upgrade:", err)
			return
		}
		defer conn.Close()

		// создание канала для отправки сообщений
		chnSend := make(chan []byte)

		wsStore.Add(conn, chnSend)
		defer func() {
			wsStore.Remove(conn)
		}()

		for {
			// Если появились данные в канале, отправляем их
			data, ok := <-chnSend
			if ok {
				err := conn.WriteMessage(websocket.TextMessage, data)
				if err != nil {
					closeErr, ok := err.(*websocket.CloseError)
					if ok && (closeErr.Code == websocket.CloseNormalClosure || closeErr.Code == websocket.CloseNoStatusReceived) {
						log.Println("Connection closed normally")
					} else {
						log.Println("Write error:", err)
					}
					return
				}
			} else {
				return
			}
		}
	}
}
