package Routes

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

// Root - "/"
func Root(c *gin.Context) {
	c.HTML(http.StatusOK, "index.html", nil)
}
