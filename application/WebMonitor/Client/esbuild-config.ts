import ESBuild from "esbuild";
import { minifyHTMLLiteralsPlugin } from "esbuild-plugin-minify-html-literals";
import path from "path";
import { fileURLToPath } from "url";
import { CleanPlugin } from "./esbuild-plugins/CleanPlugin.js";
import { HTMLPlugin } from "./esbuild-plugins/HTMLPlugin.js";
import { CSSMinifyPlugin } from "./esbuild-plugins/CSSMinifyPlugin.js";

const fileName = fileURLToPath(import.meta.url);
const dirName = path.dirname(fileName);

const OUTDIR = "./build/public/js";

let watch: boolean | ESBuild.WatchMode = null;

if (process.argv[3] === "watch") {
    watch = {
        onRebuild(error, result) {
            if (error) {
                console.error(error);
            } else {
                console.log("build");
            }
        }
    }
}

ESBuild.build({
    outdir:      OUTDIR,
    metafile:    true,
    entryPoints: ["./src/main.ts"],
    entryNames:  "[dir]/[name]-[hash]",
    minify:      true,
    bundle:      true,
    splitting:   true,
    format:      "esm",
    sourcemap:   true,
    loader:      { ".css": "text" },
    plugins:     [
        CleanPlugin,
        CSSMinifyPlugin,
        minifyHTMLLiteralsPlugin(),
        HTMLPlugin({
            input:  path.resolve(dirName, "src/index.ejs"),
            output: path.resolve(dirName, "build/index.html"),
        }),
    ],
    watch: watch,
}).catch(console.error);
