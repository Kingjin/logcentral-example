import {
    LitElement, html, css, unsafeCSS, PropertyValueMap, TemplateResult,
} from "lit";
import { customElement, state } from "lit/decorators.js";
import { repeat } from "lit/directives/repeat.js";

import _styles from "./App.css";

interface ILog {
    src: string
    time: string
    level: number
    message: string
}

function isILog(log: any): log is ILog {
    if (!(log && typeof log === "object")) return false;

    return (
        log
        && typeof log === "object"
        && typeof log.src === "string"
        && typeof log.time === "string"
        && typeof log.level === "number"
        && typeof log.message === "string"
    );
}

const svgInfo = html`
<svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 24 24">
    <path d="M12 2c5.514 0 10 4.486 10 10s-4.486 10-10 10-10-4.486-10-10 4.486-10 10-10zm0-2c-6.627 0-12 5.373-12 12s5.373 12 12 12 12-5.373 12-12-5.373-12-12-12zm-.001 5.75c.69 0 1.251.56 1.251 1.25s-.561 1.25-1.251 1.25-1.249-.56-1.249-1.25.559-1.25 1.249-1.25zm2.001 12.25h-4v-1c.484-.179 1-.201 1-.735v-4.467c0-.534-.516-.618-1-.797v-1h3v6.265c0 .535.517.558 1 .735v.999z"/>
</svg>`;

const svgWarning = html`
<svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 24 24">
    <path d="M12 5.177l8.631 15.823h-17.262l8.631-15.823zm0-4.177l-12 22h24l-12-22zm-1 9h2v6h-2v-6zm1 9.75c-.689 0-1.25-.56-1.25-1.25s.561-1.25 1.25-1.25 1.25.56 1.25 1.25-.561 1.25-1.25 1.25z"/>
</svg>`;

const svgError = html`
<svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 24 24">
    <path d="M16.142 2l5.858 5.858v8.284l-5.858 5.858h-8.284l-5.858-5.858v-8.284l5.858-5.858h8.284zm.829-2h-9.942l-7.029 7.029v9.941l7.029 7.03h9.941l7.03-7.029v-9.942l-7.029-7.029zm-8.482 16.992l3.518-3.568 3.554 3.521 1.431-1.43-3.566-3.523 3.535-3.568-1.431-1.432-3.539 3.583-3.581-3.457-1.418 1.418 3.585 3.473-3.507 3.566 1.419 1.417z"/>
</svg>`;

const mapLogLevelTemplates: Map<number, TemplateResult> = new Map([
    [0, html`
        <div class="logLevel info">
            ${ svgInfo } Info
        </div>`],
    [1, html`
        <div class="logLevel warning">
            ${ svgWarning } Warning
        </div>`],
    [2, html`
        <div class="logLevel error">
            ${ svgError } Error
        </div>`],
]);

function levelFormatter(lv: number): TemplateResult {
    if (mapLogLevelTemplates.has(lv)) {
        return mapLogLevelTemplates.get(lv);
    }
    return html`Unknown level`;
}

@customElement("webmonitor-app")
export default class App extends LitElement {
    static styles = css`${ unsafeCSS(_styles) }`;

    @state() mapLogs: Map<string, ILog[]> = new Map();

    @state() countLogs = 0;

    // constructor() {
    //     super();
    // }

    render() {
        return html`
        <div id="container">
        <header>
            <h1>LogMonitor</h1>
        </header>
        <main>
            <kj-tabber position="left">
                <kj-tabber-head>
                    ${ repeat(this.mapLogs.keys(), (src) => html`
                        <kj-tabber-tab name="${ src }">${ src } (${ this.mapLogs.get(src).length })</kj-tabber-tab>
                    `) }
                </kj-tabber-head>

                <kj-tabber-body>
                    ${ repeat(this.mapLogs.keys(), (src) => html`
                        <kj-tabber-page name="${ src }">
                            <kj-table id="table">
                                <kj-table-head>
                                    <kj-table-column name="time" width="200px" fixed-left>time</kj-table-column>
                                    <kj-table-column name="level" width="100px">level</kj-table-column>
                                    <kj-table-column name="message">message</kj-table-column>
                                </kj-table-head>
                                <kj-table-body>
                                    ${ repeat(this.mapLogs.get(src), (log) => log.time, (log) => html`
                                        <kj-table-row>
                                            <kj-table-cell slot="time">${ log.time }</kj-table-cell>
                                            <kj-table-cell slot="level">${ levelFormatter(log.level) }</kj-table-cell>
                                            <kj-table-cell slot="message">${ log.message }</kj-table-cell>
                                        </kj-table-row>
                                    `) }
                                </kj-table-body>
                            </kj-table>
                        </kj-tabber-page>
                    `) }
                </kj-tabber-body>
            </kj-tabber>
        </main>
        <footer>
            <h2>Total: ${ this.countLogs }</h2>
        </footer>
        </div>
        `;
    }

    protected firstUpdated(_changedProperties: PropertyValueMap<any> | Map<PropertyKey, unknown>): void {
        const ws = new WebSocket("ws://127.0.0.1:8700/ws");

        ws.onopen = () => {
            console.log("WS open");
        };
        ws.onclose = () => {
            console.log("WS close");
        };
        ws.onmessage = (ev: MessageEvent) => {
            console.log(`WS response: ${ ev.data }`);

            const log = JSON.parse(ev.data);

            if (isILog(log)) {
                if (!this.mapLogs.has(log.src)) {
                    this.mapLogs.set(log.src, []);
                }
                this.mapLogs.get(log.src).unshift(log);
                this.countLogs += 1;
                // this.requestUpdate("mapLogs");
            } else {
                console.error("объект log не соответствует интерфейсу ILog");
            }
        };
        ws.onerror = (ev: Event) => {
            console.log(`ERROR: ${ ev/* .data */ }`);
        };
    }
}
