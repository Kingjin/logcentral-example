import { Plugin } from "esbuild";
import { rm } from "fs/promises";
import path from "path";
import ejs from "ejs";
import fs from "fs";

import { fileURLToPath } from "url";

const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);

interface IHTMLPluginOptions {
    input: string
    output: string
}

export const HTMLPlugin = (options: IHTMLPluginOptions): Plugin => ({
    name: "HTMLPlugin",
    setup(build) {
        build.onEnd((result) => {
            console.log("Result: ", result);

            let mainScript = "";
            const outputs = result.metafile?.outputs;

            const paths = Object.keys(outputs || {});
            for (const path of paths) {
                const pathParts = path.split("/");
                const fileName = pathParts.length > 0 ? pathParts[pathParts.length - 1] : "";
                if (fileName.startsWith("main") && fileName.endsWith(".js")) {
                    pathParts.shift();
                    mainScript = pathParts.join("/");
                    break;
                }
            }

            ejs.renderFile(options.input, { mainScript }, (err, result) => {
                if (err) {
                    console.log("info", `error encountered: ${ err }`);
                    // throw err;
                } else {
                    try {
                        fs.writeFileSync(options.output, result, "utf8");
                    } catch (err) {
                        if (err) {
                            throw err;
                        }
                    }
                }
            });
        });
    },
});
