package Broker

import (
	"LogBroker/Interfaces"
	"LogMessage"
	"context"
	"errors"
	"fmt"
	"sync"
)

type Broker struct {
	srvProducer Interfaces.ILogProducerServer
	srvConsumer Interfaces.ILogConsumerServer
}

func NewBroker(srvProducer Interfaces.ILogProducerServer, srvConsumer Interfaces.ILogConsumerServer) *Broker {
	return &Broker{
		srvProducer: srvProducer,
		srvConsumer: srvConsumer,
	}
}

func (b *Broker) Run(ctx context.Context) error {
	fmt.Println("Broker is running")
	defer func() {
		fmt.Println("Broker is stopped")
	}()

	if b.srvProducer == nil {
		return errors.New(`ProducerServer is nil`)
	}
	if b.srvConsumer == nil {
		return errors.New(`ConsumerServer is nil`)
	}

	ctxCancel, cancel := context.WithCancel(ctx)
	defer cancel()

	b.srvProducer.SetReceiveHandler(b.onReceive(cancel))

	var err1, err2 error
	wg := &sync.WaitGroup{}

	wg.Add(1)
	go func() {
		defer wg.Done()
		err1 = b.srvProducer.Run(ctxCancel)
		if err1 != nil {
			cancel()
		}
	}()

	wg.Add(1)
	go func() {
		defer wg.Done()
		err2 = b.srvConsumer.Run(ctxCancel)
		if err2 != nil {
			cancel()
		}
	}()

	wg.Wait()

	if err1 != nil || err2 != nil {
		return fmt.Errorf("errors occurred: %v, %v", err1, err2)
	}
	return nil
}

func (b *Broker) onReceive(cancel context.CancelFunc) func(message LogMessage.LogMessage) {
	return func(message LogMessage.LogMessage) {
		fmt.Println("<->", message)

		err := b.srvConsumer.Send(message)
		if err != nil {
			fmt.Println("WARNING ConsumerServer: ", err)
		}
	}
}
