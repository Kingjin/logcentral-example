package main

import (
	"LogBroker/Broker"
	"LogBroker/Config"
	"LogConsumerGRPCServerAdapter"
	"LogProducerGRPCServerAdapter"
	"context"
	"fmt"
	"os/signal"
	"syscall"
	"time"
)

func main() {
	var err error
	defer func() {
		if err != nil {
			fmt.Printf("ERROR: %+v\r\n", err)
		}
		time.Sleep(time.Second)
		fmt.Println("LogBroker is finished")
	}()

	conf, err := Config.NewConfig("config.yaml")
	if err != nil {
		return
	}

	ctx, stop := signal.NotifyContext(context.Background(), syscall.SIGINT, syscall.SIGTERM)
	defer stop()

	srvProducer := LogProducerGRPCServerAdapter.NewLogProducerServer(
		conf.Producer.Name,
		conf.Producer.Network,
		conf.Producer.Address)

	srvConsumer := LogConsumerGRPCServerAdapter.NewLogConsumerServer(
		conf.Consumer.Name,
		conf.Consumer.Network,
		conf.Consumer.Address)

	broker := Broker.NewBroker(srvProducer, srvConsumer)
	err = broker.Run(ctx)
}
