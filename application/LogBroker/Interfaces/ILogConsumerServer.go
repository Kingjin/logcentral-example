package Interfaces

import (
	"LogMessage"
	"context"
)

type ILogConsumerServer interface {
	Send(LogMessage.LogMessage) error
	Run(context.Context) error
}
