package Mocks

import (
	"LogMessage"
	"context"
	"fmt"
	"math/rand"
	"time"
)

type LogProducerServer struct {
	name      string
	network   string
	address   string
	onReceive func(LogMessage.LogMessage)
}

func NewLogProducerServer(name, network, address string) *LogProducerServer {
	return &LogProducerServer{
		name:    name,
		network: network,
		address: address,
	}
}

func (s *LogProducerServer) Run(ctx context.Context) error {
	fmt.Println("[Mock] LogProducerServer", `"`+s.name+`"`, "listening on", s.address)
	defer func() {
		fmt.Println("[Mock] LogProducerServer", `"`+s.name+`"`, "is stopped")
	}()

	// Наборы мок-данных
	var srcNames = []string{"Producer 1", "Producer 2", "Producer 3"}

	var infoMessages = []string{
		"User signed up successfully",
		"Product added to cart",
		"Payment processed successfully",
		"New user registered",
		"Order placed successfully",
		"User profile updated",
		"Product review added",
		"New promotion created",
		"Invoice sent to customer",
		"Order shipped",
	}

	var warningMessages = []string{
		"User session expired",
		"Product inventory running low",
		"Invalid input data received",
		"Payment authorization failed",
		"Duplicate order detected",
		"Incorrect login credentials",
		"Unrecognized product category",
		"Email delivery failed",
		"Payment declined by bank",
		"User account temporarily suspended",
	}

	var errorMessages = []string{
		"Database connection error",
		"Internal server error",
		"Product not found",
		"Invalid authentication token",
		"Invalid request format",
		"Failed to process payment",
		"User account blocked",
		"Unauthorized access attempt",
		"Insufficient funds",
		"Critical system error",
	}

	var allMessages = [][]string{infoMessages, warningMessages, errorMessages}

	rand.Seed(time.Now().UnixNano())

	for {
		select {
		case <-ctx.Done():
			return nil

		default:
			level := int(rand.Intn(3))
			message := LogMessage.LogMessage{
				Src:     srcNames[rand.Intn(len(srcNames))],
				Time:    time.Now(),
				Level:   level,
				Message: allMessages[level][rand.Intn(len(allMessages[level]))],
			}

			if s.onReceive != nil {
				s.onReceive(message)
			}
			time.Sleep(time.Duration(rand.Intn(3)) * time.Second)
		}
	}
}

func (s *LogProducerServer) SetReceiveHandler(f func(LogMessage.LogMessage)) {
	s.onReceive = f
}

func (s *LogProducerServer) GetReceiveHandler() func(LogMessage.LogMessage) {
	return s.onReceive
}
