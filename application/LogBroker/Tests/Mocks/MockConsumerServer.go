package Mocks

import (
	"LogMessage"
	"context"

	"fmt"

	"github.com/pkg/errors"
)

type LogConsumerServer struct {
	name    string
	network string
	address string
	chnSend chan LogMessage.LogMessage
}

func NewLogConsumerServer(name, network, address string) *LogConsumerServer {
	return &LogConsumerServer{
		name:    name,
		network: network,
		address: address,
		chnSend: make(chan LogMessage.LogMessage),
	}
}

func (s *LogConsumerServer) Run(ctx context.Context) error {
	fmt.Println("[Mock] LogConsumerServer", `"`+s.name+`"`, "listening on", s.address)

	s.chnSend = make(chan LogMessage.LogMessage)
	defer func() {
		close(s.chnSend)
		fmt.Println("[Mock] LogConsumerServer", `"`+s.name+`"`, "is stopped")
	}()

	for {
		select {
		case message, ok := <-s.chnSend:
			if ok {
				fmt.Println("MockCS >>", message)
			} else {
				return errors.New("error send message")
			}
		case <-ctx.Done():
			return nil
		}
	}
}

func (s *LogConsumerServer) Send(message LogMessage.LogMessage) error {
	select {
	case s.chnSend <- message:
		return nil
	default:
		return errors.New("client not ready for sending")
	}
}
