package Tests

import (
	"LogBroker/Broker"
	"LogBroker/Config"
	"LogBroker/Tests/Mocks"
	"LogConsumerGRPCServerAdapter"
	"context"
	"fmt"
	"os/signal"
	"syscall"
	"testing"
)

func Test_RunMockProducerServer(t *testing.T) {
	fn := getFuncName()
	fmt.Println(fn, "is running")
	var err error
	defer func() {
		if err != nil {
			t.Fail()
			fmt.Printf("ERROR: %+v\r\n", err)
		}
		fmt.Println(fn, "is finished")
	}()

	conf, err := Config.NewConfig("config.yaml")
	if err != nil {
		return
	}

	ctx, stop := signal.NotifyContext(context.Background(), syscall.SIGINT, syscall.SIGTERM)
	defer stop()

	srvProducer := Mocks.NewLogProducerServer(
		conf.Producer.Name,
		conf.Producer.Network,
		conf.Producer.Address)

	srvConsumer := LogConsumerGRPCServerAdapter.NewLogConsumerServer(
		conf.Consumer.Name,
		conf.Consumer.Network,
		conf.Consumer.Address)

	broker := Broker.NewBroker(srvProducer, srvConsumer)
	err = broker.Run(ctx)
}
