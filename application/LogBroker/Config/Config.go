package Config

import (
	"os"

	"gopkg.in/yaml.v2"
)

type Config struct {
	Producer struct {
		Name    string `yaml:"name"`
		Network string `yaml:"network"`
		Address string `yaml:"address"`
	} `yaml:"producer"`
	Consumer struct {
		Name    string `yaml:"name"`
		Network string `yaml:"network"`
		Address string `yaml:"address"`
	} `yaml:"consumer"`
}

func NewConfig(path string) (*Config, error) {
	f, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	defer f.Close()

	config := &Config{}
	err = yaml.NewDecoder(f).Decode(config)
	if err != nil {
		return nil, err
	}
	return config, nil
}
