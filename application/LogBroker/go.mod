module LogBroker

go 1.18

require (
	LogConsumerGRPCServerAdapter v0.0.0
	LogMessage v0.0.0
	LogProducerGRPCServerAdapter v0.0.0
	github.com/pkg/errors v0.9.1
	gopkg.in/yaml.v2 v2.4.0
)

require (
	LogConsumerGRPCServer v0.0.0 // indirect
	LogProducerGRPCServer v0.0.0 // indirect
	PbLogMessageConverter v0.0.0 // indirect
	Sync v0.0.0 // indirect
	github.com/golang/protobuf v1.5.2 // indirect
	golang.org/x/net v0.5.0 // indirect
	golang.org/x/sys v0.4.0 // indirect
	golang.org/x/text v0.6.0 // indirect
	google.golang.org/genproto v0.0.0-20230110181048-76db0878b65f // indirect
	google.golang.org/grpc v1.53.0 // indirect
	google.golang.org/protobuf v1.28.1 // indirect
	proto v0.0.0 // indirect
)

replace (
	LogConsumerGRPCServer v0.0.0 => ../../infrastructure/grpc/LogConsumerGRPCServer
	LogConsumerGRPCServerAdapter v0.0.0 => ../../infrastructure/adapters/LogConsumerGRPCServerAdapter
	LogMessage v0.0.0 => ../../infrastructure/logging/LogMessage

	LogProducerGRPCServer v0.0.0 => ../../infrastructure/grpc/LogProducerGRPCServer
	LogProducerGRPCServerAdapter v0.0.0 => ../../infrastructure/adapters/LogProducerGRPCServerAdapter

	PbLogMessageConverter v0.0.0 => ../../infrastructure/adapters/PbLogMessageConverter
	Sync v0.0.0 => ../../shared/go/Sync
	proto v0.0.0 => ../../infrastructure/grpc/proto
)
