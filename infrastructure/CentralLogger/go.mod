module CentralLogger

go 1.18

replace (
	LogMessage v0.0.0 => ../logging/LogMessage
)

require LogMessage v0.0.0
