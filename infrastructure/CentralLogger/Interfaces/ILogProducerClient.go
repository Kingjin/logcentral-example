package Interfaces

import (
	"LogMessage"
	"context"
)

type ILogProducerClient interface {
	Send(LogMessage.LogMessage) error
	Run(context.Context) error
}
