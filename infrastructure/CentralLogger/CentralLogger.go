package CentralLogger

import (
	"CentralLogger/Interfaces"
	"LogMessage"
	"context"
	"fmt"
	"runtime"
	"time"
)

type Logger struct {
	client Interfaces.ILogProducerClient
	src    string
}

func NewLogger(client Interfaces.ILogProducerClient, src string) *Logger {
	return &Logger{
		client: client,
		src:    src,
	}
}

func (l *Logger) Run(ctx context.Context) error {
	return l.client.Run(ctx)
}

func (l *Logger) sprint(v ...any) string {
	pc /*file*/, _, line, ok := runtime.Caller(3)
	if !ok {
		//file = "?"
		line = 0
	}
	// fmt.Sprint(prefix, " ", runtime.FuncForPC(pc).Name(), " ", file, ":", line, "\n  >> ") + fmt.Sprint(v...)
	return fmt.Sprint(runtime.FuncForPC(pc).Name(), ":", line, " ") + fmt.Sprint(v...)
}

func (l Logger) Info(v ...any) {
	err := l.client.Send(LogMessage.LogMessage{
		Src:     l.src,
		Time:    time.Now(),
		Level:   0,
		Message: l.sprint(v...),
	})
	if err != nil {
		fmt.Println("Info-Log sending error:", err)
	}
}

func (l Logger) Warn(v ...any) {
	err := l.client.Send(LogMessage.LogMessage{
		Src:     l.src,
		Time:    time.Now(),
		Level:   1,
		Message: l.sprint(v...),
	})
	if err != nil {
		fmt.Println("Warn-Log sending error:", err)
	}
}

func (l Logger) Error(v ...any) {
	err := l.client.Send(LogMessage.LogMessage{
		Src:     l.src,
		Time:    time.Now(),
		Level:   2,
		Message: l.sprint(v...),
	})
	if err != nil {
		fmt.Println("Error-Log sending error:", err)
	}
}
