package Logger

type ILogger interface {
	Info(v ...any)
	Warn(v ...any)
	Error(v ...any)
}
