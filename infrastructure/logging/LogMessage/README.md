# LogMessage


## Описание

LogMessage содержит структуру лог-сообщения `LogMessage`.


## Структура `LogMessage`

```go
type LogMessage struct {
	Src     string    `json:"src"`
	Time    time.Time `json:"time"`
	Level   int       `json:"level"`
	Message string    `json:"message"`
}
```
где:
- `Src` - источник/создатель (используется для идентификации);
- `Time` - время создания;
- `Level` - уровень;
- `Message` - описание.
