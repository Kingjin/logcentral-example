package LogMessage

import "time"

type LogMessage struct {
	Src     string    `json:"src"`
	Time    time.Time `json:"time"`
	Level   int       `json:"level"`
	Message string    `json:"message"`
}

func NewInfo(src string, message string) *LogMessage {
	return &LogMessage{
		Src:     src,
		Time:    time.Now(),
		Level:   0,
		Message: message,
	}
}
