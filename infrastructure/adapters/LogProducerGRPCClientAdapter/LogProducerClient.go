package LogProducerGRPCClientAdapter

import (
	"LogMessage"
	"LogProducerGRPCClient"
	"PbLogMessageConverter"
)

type LogProducerClient struct {
	*LogProducerGRPCClient.Client
}

func NewLogProducerClient(name, target string) *LogProducerClient {
	return &LogProducerClient{
		Client: LogProducerGRPCClient.NewClient(name, target),
	}
}

func (s *LogProducerClient) Send(message LogMessage.LogMessage) error {
	return s.Client.Send(*PbLogMessageConverter.FromLogMessage(&message))
}
