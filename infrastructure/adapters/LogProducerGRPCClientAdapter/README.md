# LogProducerGRPCClientAdapter


## Описание

LogProducerGRPCClientAdapter содержит адаптер для [LogProducerGRPCClient](../../grpc/LogProducerGRPCClient).

Реализует интерфейс `ILogProducerClient`.

