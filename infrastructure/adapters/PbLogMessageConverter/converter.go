package PbLogMessageConverter

import (
	"LogMessage"
	PbLogMessage "proto/LogMessage"
	"time"
)

func ToLogMessage(pbm *PbLogMessage.LogMessage) *LogMessage.LogMessage {
	return &LogMessage.LogMessage{
		Src:     pbm.Src,
		Time:    time.Unix(pbm.Time, 0),
		Level:   int(pbm.Level),
		Message: pbm.Message,
	}
}

func FromLogMessage(m *LogMessage.LogMessage) *PbLogMessage.LogMessage {
	return &PbLogMessage.LogMessage{
		Src:     m.Src,
		Time:    m.Time.Unix(),
		Level:   int32(m.Level),
		Message: m.Message,
	}
}
