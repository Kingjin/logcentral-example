module PbLogMessageConverter

go 1.18

replace (
	LogMessage v0.0.0 => ../../logging/LogMessage
	proto v0.0.0 => ../../grpc/proto
)

require (
	LogMessage v0.0.0
	proto v0.0.0
)

require google.golang.org/protobuf v1.28.1 // indirect
