package LogConsumerGRPCClientAdapter

import (
	"LogConsumerGRPCClient"
	"LogMessage"

	"PbLogMessageConverter"

	PbMessage "proto/LogMessage"
)

type LogConsumerClient struct {
	*LogConsumerGRPCClient.Client
	onReceive func(LogMessage.LogMessage)
}

func NewLogConsumerClient(name, target string) *LogConsumerClient {
	return &LogConsumerClient{
		Client: LogConsumerGRPCClient.NewClient(target),
	}
}

func (s *LogConsumerClient) SetReceiveHandler(f func(LogMessage.LogMessage)) {
	s.onReceive = f
	s.Client.SetReceiveHandler(func(pbm PbMessage.LogMessage) {
		f(*PbLogMessageConverter.ToLogMessage(&pbm))
	})
}

func (s *LogConsumerClient) GetReceiveHandler() func(LogMessage.LogMessage) {
	return s.onReceive
}
