# LogConsumerGRPCClientAdapter


## Описание

LogConsumerGRPCClientAdapter содержит адаптер для [LogConsumerGRPCClient](../../grpc/LogConsumerGRPCClient).

Реализует интерфейс `ILogConsumerClient`.

