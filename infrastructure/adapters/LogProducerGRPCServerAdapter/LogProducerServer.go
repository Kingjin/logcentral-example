package LogProducerGRPCServerAdapter

import (
	"LogMessage"
	"LogProducerGRPCServer"
	"PbLogMessageConverter"
	PbMessage "proto/LogMessage"
)

type LogProducerServer struct {
	*LogProducerGRPCServer.Server
	onReceive func(LogMessage.LogMessage)
}

func NewLogProducerServer(name, network, address string) *LogProducerServer {
	return &LogProducerServer{
		Server: LogProducerGRPCServer.NewServer(name, network, address),
	}
}

func (s *LogProducerServer) SetReceiveHandler(f func(LogMessage.LogMessage)) {
	s.onReceive = f
	s.Server.SetReceiveHandler(func(pbm PbMessage.LogMessage) {
		f(*PbLogMessageConverter.ToLogMessage(&pbm))
	})
}

func (s *LogProducerServer) GetReceiveHandler() func(LogMessage.LogMessage) {
	return s.onReceive
}
