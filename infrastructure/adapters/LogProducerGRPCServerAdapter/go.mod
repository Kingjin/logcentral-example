module LogProducerGRPCServerAdapter

go 1.18

replace (
	LogMessage v0.0.0 => ../../logging/LogMessage
	LogProducerGRPCServer v0.0.0 => ../../grpc/LogProducerGRPCServer
	PbLogMessageConverter v0.0.0 => ../PbLogMessageConverter
	Sync v0.0.0 => ../../../shared/go/Sync
	proto v0.0.0 => ../../grpc/proto
)

require (
	LogMessage v0.0.0
	LogProducerGRPCServer v0.0.0
	PbLogMessageConverter v0.0.0
	proto v0.0.0
)

require (
	github.com/golang/protobuf v1.5.2 // indirect
	golang.org/x/net v0.5.0 // indirect
	golang.org/x/sys v0.4.0 // indirect
	golang.org/x/text v0.6.0 // indirect
	google.golang.org/genproto v0.0.0-20230110181048-76db0878b65f // indirect
	google.golang.org/grpc v1.53.0 // indirect
	google.golang.org/protobuf v1.28.1 // indirect
)
