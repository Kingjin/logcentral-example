# LogProducerGRPCServerAdapter


## Описание

LogProducerGRPCServerAdapter содержит адаптер для [LogProducerGRPCServer](../../grpc/LogProducerGRPCServer).

Реализует интерфейс `ILogProducerServer`.

