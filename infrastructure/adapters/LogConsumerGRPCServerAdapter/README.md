# LogConsumerGRPCServerAdapter


## Описание

LogConsumerGRPCServerAdapter содержит адаптер для [LogConsumerGRPCServer](../../grpc/LogConsumerGRPCServer).

Реализует интерфейс `ILogConsumerServer`.

