package LogConsumerGRPCServerAdapter

import (
	"LogConsumerGRPCServer"
	"LogMessage"
	"PbLogMessageConverter"
)

type LogConsumerServer struct {
	*LogConsumerGRPCServer.Server
}

func NewLogConsumerServer(name, network, address string) *LogConsumerServer {
	return &LogConsumerServer{
		Server: LogConsumerGRPCServer.NewServer(name, network, address),
	}
}

func (s *LogConsumerServer) Send(message LogMessage.LogMessage) error {
	return s.Server.Send(*PbLogMessageConverter.FromLogMessage(&message))
}
