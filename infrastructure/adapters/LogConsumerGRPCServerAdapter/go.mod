module LogConsumerGRPCServerAdapter

go 1.18

replace (
	LogConsumerGRPCServer v0.0.0 => ../../grpc/LogConsumerGRPCServer
	LogMessage v0.0.0 => ../../logging/LogMessage
	PbLogMessageConverter v0.0.0 => ../PbLogMessageConverter
	Sync v0.0.0 => ../../../shared/go/Sync
	proto v0.0.0 => ../../grpc/proto
)

require (
	LogConsumerGRPCServer v0.0.0
	LogMessage v0.0.0
	PbLogMessageConverter v0.0.0
)

require (
	Sync v0.0.0 // indirect
	github.com/golang/protobuf v1.5.2 // indirect
	golang.org/x/net v0.5.0 // indirect
	golang.org/x/sys v0.4.0 // indirect
	golang.org/x/text v0.6.0 // indirect
	google.golang.org/genproto v0.0.0-20230110181048-76db0878b65f // indirect
	google.golang.org/grpc v1.53.0 // indirect
	google.golang.org/protobuf v1.28.1 // indirect
	proto v0.0.0 // indirect
)
