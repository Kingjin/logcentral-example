package LogProducerGRPCClient

import (
	"context"
	"errors"
	"sync"
	"time"

	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"

	PbMessage "proto/LogMessage"
	PbProducer "proto/LogProducer"
)

var ErrClientAlreadyRunning = errors.New("LogProducerClient is already running")

// Client - структура LogProducer-клиента
type Client struct {
	name   string
	target string
	mu     sync.Mutex
	ctx    context.Context
	chnErr chan error
	conn   *grpc.ClientConn
	client PbProducer.LogProducerServiceClient
}

// NewClient создаёт новый экземпляр LogProducer-клиента
func NewClient(name, target string) *Client {
	return &Client{
		name:   name,
		target: target,
	}
}

// Run устанавливает gRPC-соединение с LogProducer-сервером и запускает цикл передачи сообщений.
// Блокирует поток выполнения.
func (c *Client) Run(ctx context.Context) error {
	c.mu.Lock()
	if c.conn != nil {
		c.mu.Unlock()
		return ErrClientAlreadyRunning
	}

	var err error
	c.conn, err = grpc.Dial(c.target, grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		c.mu.Unlock()
		return err
	}

	c.ctx = ctx
	c.client = PbProducer.NewLogProducerServiceClient(c.conn)
	c.chnErr = make(chan error)
	c.mu.Unlock()
	defer c.Stop()

	for {
		select {
		case err = <-c.chnErr:
			if err != nil {
				return err
			}
		case <-ctx.Done():
			return nil
		}
	}
}

// Stop останавливает работу Клиента
func (c *Client) Stop() {
	c.mu.Lock()
	defer c.mu.Unlock()

	if c.conn == nil {
		return
	}

	c.client = nil
	close(c.chnErr)
	c.conn.Close()
	c.conn = nil
}

// Send отправляет сообщение Серверу
func (c *Client) Send(message PbMessage.LogMessage) error {
	c.mu.Lock()
	defer c.mu.Unlock()

	if c.client == nil {
		return errors.New("Client not running")
	}

	ctx, cancel := context.WithTimeout(c.ctx, 3*time.Second)
	defer cancel()

	_, err := c.client.Log(ctx, &message)
	if err != nil {
		select {
		case c.chnErr <- err:
			break
		default:
		}
	}
	return err
}
