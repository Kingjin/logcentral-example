package LogConsumerGRPCClient

import (
	"context"
	"io"
	"sync"

	PbConsumer "proto/LogConsumer"
	PbMessage "proto/LogMessage"

	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/credentials/insecure"
	"google.golang.org/grpc/status"
)

// Client - структура LogConsumer-клиента
type Client struct {
	mu        sync.RWMutex
	target    string
	onReceive func(PbMessage.LogMessage)
	//onReceive atomic.Value
}

// NewClient создаёт новый экземпляр LogConsumer-клиента
func NewClient(target string) *Client {
	return &Client{
		target: target,
	}
}

// Run устанавливает gRPC-соединение с LogConsumer-сервером и выполняет приём сообщений.
// Блокирует поток выполнения.
func (c *Client) Run(ctx context.Context) error {
	if ctx.Err() != nil {
		return nil
	}

	conn, err := grpc.Dial(c.target, grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		return err
	}
	defer conn.Close()

	client := PbConsumer.NewLogConsumerServiceClient(conn)
	stream, err := client.LogStream(ctx)
	if err != nil {
		return err
	}

	for {
		select {
		case <-ctx.Done():
			return nil
		default:
			message, err := stream.Recv()
			if err != nil {
				if err == io.EOF || status.Code(err) == codes.Canceled {
					return nil
				}
				return err
			}

			c.mu.RLock()
			if c.onReceive != nil {
				c.onReceive(*message)
			}
			c.mu.RUnlock()
			//(c.onReceive.Load().(func(PbMessage.LogMessage)))(*message)
		}
	}
}

// SetReceiveHandler назначает callback-функцию получения сообщений
func (c *Client) SetReceiveHandler(f func(PbMessage.LogMessage)) {
	c.mu.Lock()
	c.onReceive = f
	c.mu.Unlock()
	//c.onReceive.Store(f)
}

// GetReceiveHandler возвращает callback-функцию получения сообщений
func (c *Client) GetReceiveHandler() func(PbMessage.LogMessage) {
	c.mu.RLock()
	defer c.mu.RUnlock()
	return c.onReceive
	//return c.onReceive.Load().(func(PbMessage.LogMessage))
}
