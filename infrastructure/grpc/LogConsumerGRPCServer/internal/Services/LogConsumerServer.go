package Services

import (
	"fmt"
	pbConsumer "proto/LogConsumer"
)

type ILogConsumerStream = pbConsumer.LogConsumerService_LogStreamServer
type ConsumerStreamFunc = func(stream *ILogConsumerStream)

type LogConsumerServer struct {
	onConnect    ConsumerStreamFunc
	onDisconnect ConsumerStreamFunc
}

func NewLogConsumerServer(onConnect ConsumerStreamFunc, onDisconnect ConsumerStreamFunc) *LogConsumerServer {
	return &LogConsumerServer{
		onConnect:    onConnect,
		onDisconnect: onDisconnect,
	}
}

func (s *LogConsumerServer) LogStream(stream ILogConsumerStream) error {
	if s.onConnect != nil {
		s.onConnect(&stream)
	}
	defer func() {
		if s.onDisconnect != nil {
			s.onDisconnect(&stream)
		}
	}()

	<-stream.Context().Done()

	fmt.Println("Client disconnected")
	return nil
	// for {
	// 	// вместо использования stream.Recv(), выполняется проверка состояние соединения
	// 	if stream.Context().Err() != nil {
	// 		log.Println("Client disconnected")
	// 		return nil
	// 	}

	// 	// ожидание перед повторной проверкой состояния соединения
	// 	time.Sleep(time.Second)
	// }
}
