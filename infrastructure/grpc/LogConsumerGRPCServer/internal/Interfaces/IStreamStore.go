package Interfaces

// IStreamStore - интерфейс хранилища gRPC-стримов получателей (consumers)
type IStreamStore[T comparable] interface {
	Add(stream T)
	Remove(stream T)
	ForEach(f func(T) error) error
}
