package StreamStore

import Sync "Sync/Set"

type StreamStore[T comparable] struct {
	s *Sync.Set[T]
}

func NewStreamStore[T comparable]() *StreamStore[T] {
	return &StreamStore[T]{
		s: Sync.NewSet[T](),
	}
}

func (ss *StreamStore[T]) Add(stream T) {
	ss.s.Add(stream)
}

func (ss *StreamStore[T]) Remove(stream T) {
	ss.s.Remove(stream)
}

func (ss *StreamStore[T]) ForEach(f func(T) error) error {
	return ss.s.ForEach(f)
}
