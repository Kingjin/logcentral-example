package LogConsumerGRPCServer

import (
	"context"
	"fmt"
	"net"

	PbConsumer "proto/LogConsumer"
	PbMessage "proto/LogMessage"

	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"

	"LogConsumerGRPCServer/internal/Interfaces"
	"LogConsumerGRPCServer/internal/Services"
	"LogConsumerGRPCServer/internal/StreamStore"
)

type Server struct {
	name        string
	network     string
	address     string
	streamStore Interfaces.IStreamStore[*Services.ILogConsumerStream]
}

func NewServer(name, network, address string) *Server {
	return &Server{
		name:        name,
		network:     network,
		address:     address,
		streamStore: StreamStore.NewStreamStore[*Services.ILogConsumerStream](),
	}
}

func (s *Server) Run(ctx context.Context) error {
	fmt.Println("gRPC-ConsumerServer", `"`+s.name+`"`, "is running")
	gs := grpc.NewServer()
	defer func() {
		gs.GracefulStop()
		fmt.Println("gRPC-ConsumerServer", `"`+s.name+`"`, "is stopped")
	}()

	PbConsumer.RegisterLogConsumerServiceServer(gs, Services.NewLogConsumerServer(s.onConnect, s.onDisconnect))
	reflection.Register(gs)

	lis, err := net.Listen(s.network, s.address)
	if err != nil {
		return err
	}
	fmt.Println("gRPC-ConsumerServer", `"`+s.name+`"`, "listening on", lis.Addr())

	chnErr := make(chan error)
	defer close(chnErr)

	go func() {
		err := gs.Serve(lis)
		if err != nil {
			chnErr <- err
		}
	}()

	select {
	case err := <-chnErr:
		return err
	case <-ctx.Done():
		return nil
	}
}

func (s *Server) onConnect(stream *Services.ILogConsumerStream) {
	s.streamStore.Add(stream)
}

func (s *Server) onDisconnect(stream *Services.ILogConsumerStream) {
	s.streamStore.Remove(stream)
}

func (s *Server) Send(message PbMessage.LogMessage) error {
	return s.streamStore.ForEach(func(stream *Services.ILogConsumerStream) error {
		return (*stream).Send(&message)
	})
}
