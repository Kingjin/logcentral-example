package Services

import (
	"context"

	"github.com/golang/protobuf/ptypes/empty"

	PbMessage "proto/LogMessage"
)

type LogProducerServer struct {
	receiveF *ReceiveF
}

func NewLogProducerServer(receiveF *ReceiveF) *LogProducerServer {
	return &LogProducerServer{
		receiveF: receiveF,
	}
}

func (s *LogProducerServer) Log(ctx context.Context, message *PbMessage.LogMessage) (*empty.Empty, error) {
	if s.receiveF.GetF() != nil {
		s.receiveF.GetF()(*message)
	}

	return &empty.Empty{}, nil
}
