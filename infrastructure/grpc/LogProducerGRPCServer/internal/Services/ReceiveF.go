package Services

import (
	"sync"

	PbMessage "proto/LogMessage"
)

// ReceiveF - структур-обёртка для callback-функции получения сообщения
type ReceiveF struct {
	mu sync.RWMutex
	f  func(PbMessage.LogMessage)
}

func (r *ReceiveF) SetF(f func(PbMessage.LogMessage)) {
	r.mu.Lock()
	defer r.mu.Unlock()
	r.f = f
}

func (r *ReceiveF) GetF() func(PbMessage.LogMessage) {
	r.mu.RLock()
	defer r.mu.RUnlock()
	return r.f
}
