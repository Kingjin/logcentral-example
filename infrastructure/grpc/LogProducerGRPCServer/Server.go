package LogProducerGRPCServer

import (
	"context"
	"fmt"
	"net"

	PbMessage "proto/LogMessage"
	PbProducer "proto/LogProducer"

	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"

	"LogProducerGRPCServer/internal/Services"
)

type Server struct {
	name     string
	network  string
	address  string
	receiveF Services.ReceiveF
}

func NewServer(name, network, address string) *Server {
	return &Server{
		name:    name,
		network: network,
		address: address,
	}
}

func (s *Server) Run(ctx context.Context) error {
	fmt.Println("gRPC-ProducerServer", `"`+s.name+`"`, "is running")
	gs := grpc.NewServer()
	defer func() {
		gs.GracefulStop()
		fmt.Println("gRPC-ProducerServer", `"`+s.name+`"`, "is stopped")
	}()

	PbProducer.RegisterLogProducerServiceServer(gs, Services.NewLogProducerServer(&s.receiveF))
	reflection.Register(gs)

	lis, err := net.Listen(s.network, s.address)
	if err != nil {
		return err
	}
	fmt.Println("gRPC-ProducerServer", `"`+s.name+`"`, "listening on", lis.Addr())

	chnErr := make(chan error)
	defer close(chnErr)

	go func() {
		err := gs.Serve(lis)
		if err != nil {
			chnErr <- err
		}
	}()

	select {
	case err := <-chnErr:
		return err
	case <-ctx.Done():
		return nil
	}
}

func (s *Server) SetReceiveHandler(f func(PbMessage.LogMessage)) {
	s.receiveF.SetF(f)
}

func (s *Server) GetReceiveHandler() func(PbMessage.LogMessage) {
	return s.receiveF.GetF()
}
