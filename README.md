
<div align="center">
  <img src="docs/logo.svg" />

# LogCentral-Example
</div>

[[_TOC_]]

## Описание

**LogCentral-Example** - это пример реализации <ins>подсистемы</ins> централизованного логирования, предназначенной для сбора и обработки лог-сообщений от различных компонентов системы.


### Термины

- **Лог-сообщение** (**LogMessage**) - структура данных, которой оперируют основные компоненты (Отправитель, Брокер, Получатель);
- **Отправитель** (**Producer**, "лог-клиент") - компонент, который выполняет логирование (создание и отправку лог-сообщений Брокеру);
- **Получатель** (**Consumer**, "лог-сервер") - компонент, который получает лог-сообщения от Брокера для дальнейшей обработки, анализа или хранения;
- **Брокер** (**Broker**) - компонент, который выполняет маршрутизацию лог-сообщений от Отправителей к Получателям на основе определенных правил фильтрации.


### Интеграция

Точкой интеграции с другими системами является специальный логгер ([`CentralLogger`](infrastructure/CentralLogger)), реализующий интерфейс [`ILogger`](infrastructure/logging/Logger/ILogger.go) (используется три уровня логов):

```go
type ILogger interface {
	Info(v ...any)
	Warn(v ...any)
	Error(v ...any)
}
```


### Структура проекта

Проект представляет собой тестовый стенд, в основе которого лежит мок-система с трёхслойной архитектурой:
- **[Application layer](application/)** (слой приложений) - содержит три приложения:
    - **[LogBroker](application/LogBroker)** (брокер логов) - центральный микросервис сбора и перенаправления логов.
    - **[LogGenerator](application/LogGenerator)** (генератор логов) - микросервис, имитирующий работу произвольного микросервиса в мок-системе, который выполняет логирование. Поддерживает запуск нескольких экземпляров (у каждого будет свой "маркер" по времени запуска, для идентификации в `WebMonitor`).
    - **[WebMonitor](application/WebMonitor)** (веб-монитор) - микросервис, работающий в режиме веб-сервера, для отображения полученных логов в реальном времени.
- **[Infrastructure layer](infrastructure/)** (слой инфраструктуры) - содержит компоненты для реализации обмена лог-сообщениями между микросервисами системы.
- **[Domain layer](domain/)** (слой домена) - пустой, т.к. проект не содержит какую-либо бизнес-логику.


### Roadmap

Что планируется добавить:

- [ ] Фильтрация лог-сообщений.
  > Вариант реализации: фильтрацию реализовать двухстороннюю - со стороны лог-клиента (кому и что отправлять), так и со стороны лог-сервера (от кого и что принимать). Условия фильтров передавать Брокеру, на основании которых, он будет проводить маршрутизацию лог-сообщений.
- [ ] Очередь или буферизация лог-сообщений.
  > Вариант реализации: отдельным компонентом (можно использовать Kafka, RabbitMQ или другие подобные технологии), который будет хранить недоставленные лог-сообщения. Отдельные экземпляры использовать на лог-клиентах и Брокере.
- [ ] Web-интерфейс Брокера, для отображения метрик его состояния.
  > Вариант реализации: добавить в Брокер веб-сервер и написать SPA с графическими компонентами, для отображением метрик в реальном времени (или использовать Prometheus + Grafana).

<br>
<br>


## Диаграмма взаимодействия между компонентами

> Внутреннее взаимодействие (отправка/получение лог-сообщений) между компонентами реализовано с помощью gRPC.

<br>
<div align="center">
  <img src="docs/context.svg" />
</div>

Компоненты:
- **LogBroker** - брокер логов, выполняет централизованный сбор логов от Отправителей и перенаправляет их Получателям для дальнейшей обработки и анализа;
- **LogProducer** [1..N] - Отправители логов;
- **LogConsumer** [1..M] - Получатели логов.
<br>
<br>

## Структура каталогов

<br>
<div align="center">
  <img src="docs/dirs.svg" />
</div>
<br>
<br>

## Диаграмма компонентов и их зависимостей

<br>
<div align="center">
  <img src="docs/components.svg" />
</div>
<br>

- **[LogGenerator](application/LogGenerator)** - микросервис, представляющий из себя имитацию LogProducer'а, который генерирует рандомные лог-сообщения с разными уровнями и разным временем создания.
- **[LogBroker](application/LogBroker)** - микросервис брокера логов, который выполняет централизованный сбор логов от Отправителей и перенаправляет их Получателям для дальнейшей обработки и анализа.
- **[WebMonitor](application/WebMonitor)** - веб-приложение для получения и отображение логов в режиме реального времени.
- **[ILogger](infrastructure/logging/Logger/ILogger.go)** - интерфейс логгера.
- **[CentralLogger](infrastructure/CentralLogger)** - модуль структуры `CentralLogger`. Реализует интерфейс `ILogger`.
- **[ILogProducerClient](infrastructure/CentralLogger/Interfaces/ILogProducerClient.go)** - интерфейс клиента Отправителя.
- **[ILogProducerServer](application/LogBroker/Interfaces/ILogProducerServer.go)** - интерфейс сервера Отправителя.
- **[ILogConsumerClient](application/WebMonitor/Server/Interfaces/ILogConsumerClient.go)** - интерфейс клиента Получателя.
- **[ILogConsumerServer](application/LogBroker/Interfaces/ILogConsumerServer.go)** - интерфейс сервера Получателя.
- **[LogMessage](infrastructure/logging/LogMessage)** - модуль структуры [`LogMessage`](infrastructure/logging/LogMessage/LogMessage.go).
- **[LogProducerGRPCClientAdapter](infrastructure/adapters/LogProducerGRPCClientAdapter)** - модуль адаптера для [LogProducerGRPCClient](infrastructure/grpc/LogProducerGRPCClient). Реализует интерфейс `ILogProducerClient`.
- **[LogProducerGRPCServerAdapter](infrastructure/adapters/LogProducerGRPCServerAdapter)** - модуль адаптера для [LogProducerGRPCServer](infrastructure/grpc/LogProducerGRPCServer). Реализует интерфейс `ILogProducerServer`.
- **[LogConsumerGRPCClientAdapter](infrastructure/adapters/LogConsumerGRPCClientAdapter)** - модуль адаптера для [LogConsumerGRPCClient](infrastructure/grpc/LogConsumerGRPCClient). Реализует интерфейс `ILogConsumerClient`.
- **[LogConsumerGRPCServerAdapter](infrastructure/adapters/LogConsumerGRPCServerAdapter)** - модуль адаптера для [LogConsumerGRPCServer](infrastructure/grpc/LogConsumerGRPCServer). Реализует интерфейс `ILogConsumerServer`.
- **[PbLogMessageConverter](infrastructure/adapters/PbLogMessageConverter)** - модуль, содержащий функции конвертирования структуры `proto/LogMessage` в `LogMessage/LogMessage` и обратно.
- **[LogProducerGRPCClient](infrastructure/grpc/LogProducerGRPCClient)** - модуль, содержащий реализацию клиента для взаимодействия с сервером LogProducer, работающем через gRPC.
- **[LogProducerGRPCServer](infrastructure/grpc/LogProducerGRPCServer)** - модуль, содержащий реализацию сервера для обработки запросов от клиентов, работающих с LogProducer через gRPC.
- **[LogConsumerGRPCClient](infrastructure/grpc/LogConsumerGRPCClient)** - модуль, содержащий реализацию клиента для взаимодействия с сервером LogConsumer, работающем через gRPC.
- **[LogConsumerGRPCServer](infrastructure/grpc/LogConsumerGRPCServer)** - модуль, содержащий реализацию сервера для обработки запросов от клиентов, работающих с LogConsumer через gRPC.
- **[proto](infrastructure/grpc/proto)** - модуль, содержащий protobuf-схему сообщения `LogMessage` и файлы с описанием API для `LogProducer` и `LogConsumer`.
<br>
<br>


## Сборка и запуск

**Зависимости:** `Go` (>= 1.18) и `NodeJS` (подойдёт любая не слишком устаревшая версия).

Для сборки основных приложений проекта выполните `make build` или просто `make`.

После успешной сборки всех приложений, их можно запустить из своих директорий в любой последовательности, но с определённым количеством экземпляров:
- `LogBroker` = 1
- `WebMonitor` = 1
- `LogGenerator` >=1

Например, при одном `LogGenerator`:

<br>
<div align="center">
  <img src="docs/genexe1.png" />
</div>
<br>

При запуске ещё двух `LogGenerator` в WebMonitor появятся дополнительные вкладки:

<br>
<div align="center">
  <img src="docs/genexe3.png" />
</div>
<br>