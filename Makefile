#.ONESHELL


build:
	cd ./application/LogBroker && make tidy build
	cd ./application/LogGenerator && make tidy build
	cd ./application/WebMonitor/Server && make tidy build
	cd ./application/WebMonitor/Client && npm i && npm run build
	cd ./shared/ts/KjLib/Lit && npm i
